# PadUi

Angular Component UI Library based on Untitled UI Kit

## ✨ Features

- Easy to install and use.
- Many UI components can be used to build breathtaking applications in no time.
- Written in TypeScript with predictable static types.
- Multiple entry points support of treeshaking, smaller bundle sizes.
- Powerful theme customization in every detail.

## 🎨 Design Specification

`pad-ui` synchronizes design specification with [Untitled UI](https://www.untitledui.com/)

## 📦 Installation

```bash
$ ng new PROJECT_NAME
$ cd PROJECT_NAME
$ npm i pad-ui-lib
```

## 🔨 Usage

Import the component modules you want to use into your `app.module.ts` file and [feature modules](https://angular.io/guide/feature-modules).

```ts
import { PadButtonModule } from "pad-ui-lib";

@NgModule({
  imports: [PadButtonModule],
})
export class AppModule {}
```

> `@angular/cli` users won't have to worry about the things below but it's good to know.
> And import style and SVG icon assets file link in `angular.json`.

```diff
{
  "assets": [
+   {
+     "glob": "**/*",
+     "input": "./node_modules/pad-ui-lib/assets",
+     "output": "/assets/"
+   }
  ],
  "styles": [
+   "node_modules/pad-ui-lib/styles/index.scss"
  ]
}
```
