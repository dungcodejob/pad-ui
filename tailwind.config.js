/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,ts}"],
  theme: {
    extend: {
      colors: {
        "pad-gray": {
          50: "var(--pad-gray-50)",
          100: "var(--pad-gray-100)",
          200: "var(--pad-gray-200)",
          300: "var(--pad-gray-300)",
          400: "var(--pad-gray-400)",
          500: "var(--pad-gray-500)",
          600: "var(--pad-gray-600)",
          700: "var(--pad-gray-700)",
          800: "var(--pad-gray-800)",
          900: "var(--pad-gray-900)",
        },
      },
    },
  },
  plugins: [],
};
