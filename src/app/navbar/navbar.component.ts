import { Component, inject } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.scss"],
})
export class NavbarComponent {
  private readonly _router = inject(Router);
  goToSelect() {
    this._router.navigate(["/select"]);
  }

  goToInput() {
    this._router.navigate(["/input"]);
  }

  goToRadio() {
    this._router.navigate(["/radio"]);
  }
  goToMenu() {
    this._router.navigate(["/menu"]);
  }
  goToDialog() {
    this._router.navigate(["/dialog"]);
  }
  goToToast() {
    this._router.navigate(["/toast"]);
  }
  goToButton() {
    this._router.navigate(["/button"]);
  }
  goToAutocomplete() {
    this._router.navigate(["/autocomplete"]);
  }
}
