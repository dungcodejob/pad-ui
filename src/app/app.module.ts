import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { PadDialogModule } from "pad-ui-lib/dialog";
import { PadFormModule } from "pad-ui-lib/form";
import { PadIconModule } from "pad-ui-lib/icon";
import { PadInputModule } from "pad-ui-lib/input-number";
import { PadSelectModule } from "pad-ui-lib/select";
import { PadToastModule } from "pad-ui-lib/toast";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { DialogComponent } from "./dialog/dialog.component";
import { NavbarComponent } from "./navbar/navbar.component";
@NgModule({
  declarations: [AppComponent, DialogComponent, NavbarComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PadDialogModule,
    PadFormModule,
    PadInputModule,
    PadIconModule.forRoot(),
    PadToastModule.forRoot(),
    BrowserAnimationsModule,
    PadSelectModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
