import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  {
    path: "select",
    loadComponent: () => import("./demo/select-demo/select-demo.component"),
  },
  {
    path: "input",
    loadComponent: () => import("./demo/input-demo/input-demo.component"),
  },
  {
    path: "button",
    loadComponent: () => import("./demo/button-demo/button-demo.component"),
  },
  {
    path: "radio",
    loadComponent: () => import("./demo/radio-demo/radio-demo.component"),
  },
  {
    path: "menu",
    loadComponent: () => import("./demo/menu-demo/menu-demo.component"),
  },
  {
    path: "dialog",
    loadComponent: () => import("./demo/dialog-demo/dialog-demo.component"),
  },
  {
    path: "toast",
    loadComponent: () => import("./demo/toast-demo/toast-demo.component"),
  },
  {
    path: "autocomplete",
    loadComponent: () => import("./demo/autocomplete-demo/autocomplete-demo.component"),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
