import { Component, OnInit, inject } from "@angular/core";
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";

import { DialogConfig, PadDialogService } from "pad-ui-lib/dialog";
import { PadToastService } from "pad-ui-lib/toast";
import { combineLatest, startWith } from "rxjs";
import { DialogComponent } from "./dialog/dialog.component";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  readonly #fb = inject(FormBuilder);
  #dialogService = inject(PadDialogService);
  #toastService = inject(PadToastService);
  title = "pad-ui";
  value = 0;
  i = new FormGroup({
    value1: new FormControl<number | null>(1000),
    value2: new FormControl<number | null>(1000),
    valueSum: new FormControl<number | null>(1000),
  });

  form!: FormGroup<{
    username: FormControl<string>;
    password: FormControl<string>;
  }>;
  ngOnInit(): void {
    const value1Control = this.i.controls.value1;
    const value2Control = this.i.controls.value2;
    const valueSumControl = this.i.controls.valueSum;

    const value1$ = this.i.controls.value1.valueChanges.pipe(
      startWith(value1Control.value)
    );
    const value2$ = this.i.controls.value2.valueChanges.pipe(
      startWith(value2Control.value)
    );

    combineLatest([value1$, value2$]).subscribe(([value1, value2]) => {
      valueSumControl.setValue((value1 ?? 0) + (value2 ?? 0));
    });

    this.form = this.#fb.group({
      username: this.#fb.control("", {
        nonNullable: true,
        validators: [Validators.required],
      }),
      password: this.#fb.control("", {
        nonNullable: true,
        validators: [Validators.required],
      }),
    });
  }

  openDialog() {
    const config = new DialogConfig();
    config.closable = true;
    this.#dialogService.open(DialogComponent, config);
  }
  showToast() {
    this.#toastService.warning({
      title: "Notification Title",
      message:
        "This is the content of the notification. This is the content of the notification. This is the content of the notification.",
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      Object.values(this.form.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          // control.markAsTouched();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    } else {
      this.form.reset({ username: "", password: "" });
      this.form.controls.password.markAsUntouched();
      this.form.controls.username.markAsUntouched();
    }
  }

  #validateForm(form: AbstractControl) {
    if (form.valid) {
      return;
    }

    if (form instanceof FormControl) {
      form.markAsDirty();
      form.updateValueAndValidity({ onlySelf: true });
    } else if (form instanceof FormGroup) {
      Object.values(this.form.controls).forEach(control => {
        this.#validateForm(control);
      });
    } else if (form instanceof FormArray) {
      form.controls.forEach(control => {
        this.#validateForm(control);
      });
    }
  }
}
