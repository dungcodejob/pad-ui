import { Component } from "@angular/core";
import { PadDialogModule } from "pad-ui-lib/dialog";

@Component({
  templateUrl: "./dialog-demo.component.html",
  styleUrls: ["./dialog-demo.component.scss"],
  standalone: true,
  imports: [PadDialogModule],
})
class DialogDemoComponent {}

export default DialogDemoComponent;
