import { ComponentFixture, TestBed } from "@angular/core/testing";

import { ToastDemoComponent } from "./toast-demo.component";

describe("ToastDemoComponent", () => {
  let component: ToastDemoComponent;
  let fixture: ComponentFixture<ToastDemoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ToastDemoComponent],
    });
    fixture = TestBed.createComponent(ToastDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
