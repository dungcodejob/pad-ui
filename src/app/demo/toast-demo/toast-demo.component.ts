import { Component, inject } from "@angular/core";
import { PadButtonModule } from "pad-ui-lib/button";
import { PadToastModule, PadToastService } from "pad-ui-lib/toast";

@Component({
  templateUrl: "./toast-demo.component.html",
  styleUrls: ["./toast-demo.component.scss"],
  standalone: true,
  imports: [PadToastModule, PadButtonModule],
})
class ToastDemoComponent {
  private readonly _toastService = inject(PadToastService);
  private readonly _message = "Số lượng sản phẩm không thể bé hơn không";
  private readonly _title = "Cập nhật sản phẩm thất bại";

  onShowToastError() {
    this._toastService.error({
      message: this._message,
    });
  }

  onShowToastSuccess() {
    this._toastService.success({
      message: this._message,
    });
  }

  onShowToastWarning() {
    this._toastService.warning({
      message: this._message,
    });
  }
  onShowToastInfo() {
    this._toastService.info({
      message: this._message,
    });
  }

  onShowToastErrorWithTitle() {
    this._toastService.error({
      title: this._title,
      message: this._message,
    });
  }

  onShowToastSuccessWithTitle() {
    this._toastService.success({
      title: this._title,
      message: this._message,
    });
  }

  onShowToastWarningWithTitle() {
    this._toastService.warning({
      title: this._title,
      message: this._message,
    });
  }

  onShowToastInfoWithTitle() {
    this._toastService.info({
      title: this._title,
      message: this._message,
    });
  }

  onShowToastCustomWidth() {
    this._toastService.info({
      width: "30rem",
      title: this._title,
      message: this._message,
    });
  }
}

export default ToastDemoComponent;
