import { CdkMenuModule } from "@angular/cdk/menu";
import { Component } from "@angular/core";
import { PadButtonModule } from "pad-ui-lib/button";
import { PadIconModule } from "pad-ui-lib/icon";
import { PadMenuModule } from "pad-ui-lib/menu";
@Component({
  templateUrl: "./menu-demo.component.html",
  styleUrls: ["./menu-demo.component.scss"],
  standalone: true,
  imports: [PadMenuModule, PadButtonModule, CdkMenuModule, PadIconModule],
})
export class MenuDemoComponent {}

export default MenuDemoComponent;
