import { Component } from "@angular/core";
import { FormControl, ReactiveFormsModule } from "@angular/forms";
import { PadRadioModule } from "pad-ui-lib/radio/radio.module";

@Component({
  templateUrl: "./radio-demo.component.html",
  styleUrls: ["./radio-demo.component.scss"],
  standalone: true,
  imports: [PadRadioModule, ReactiveFormsModule],
})
export class RadioDemoComponent {
  languageControl = new FormControl("");
  planControl = new FormControl("basic");
}

export default RadioDemoComponent;
