import { AsyncPipe, NgFor } from "@angular/common";
import { Component, OnInit } from "@angular/core";
import { FormControl, ReactiveFormsModule } from "@angular/forms";
import {
  AutocompleteDataSourceItem,
  PadAutocompleteModule,
} from "pad-ui-lib/autocomplete";
import { PadFormModule } from "pad-ui-lib/form";
import { PadMenuModule } from "pad-ui-lib/menu";
import { Observable, map, startWith } from "rxjs";

@Component({
  templateUrl: "./autocomplete-demo.component.html",
  styleUrls: ["./autocomplete-demo.component.scss"],
  standalone: true,
  imports: [
    NgFor,
    AsyncPipe,
    ReactiveFormsModule,
    PadFormModule,
    PadAutocompleteModule,
    PadMenuModule,
  ],
})
export default class AutocompleteComponent implements OnInit {
  options: AutocompleteDataSourceItem[] = [
    { value: "1", label: "Anh Dung" },
    { value: "2", label: "Thành Lộc" },
    { value: "3", label: "Nguyễn thanh thuỷ" },
    { value: "4", label: "Đào Vĩnh Khiêm" },
    { value: "5", label: "Trương Bá Quốc" },
    { value: "6", label: "Nguyễn thị Linh" },
    { value: "7", label: "Huỳnh Bá Tuấn" },
  ];
  searchControl = new FormControl({ value: "1", label: "Anh Dung" });

  filterOptions$!: Observable<AutocompleteDataSourceItem[]>;

  ngOnInit(): void {
    this.filterOptions$ = this.searchControl.valueChanges.pipe(
      startWith(""),
      map(() => this.options)
    );

    this.searchControl.valueChanges.subscribe(value => console.log(value));
  }

  compareFun = (
    o1: AutocompleteDataSourceItem | string,
    o2: AutocompleteDataSourceItem
  ): boolean => {
    if (o1) {
      return typeof o1 === "string" ? o1 === o2.label : o1.value === o2.value;
    } else {
      return false;
    }
  };
}
