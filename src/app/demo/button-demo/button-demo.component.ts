import { NgFor } from "@angular/common";
import { Component, signal } from "@angular/core";
import {
  PadButtonColor,
  PadButtonModule,
  PadButtonSize,
  PadButtonStyle,
} from "pad-ui-lib/button";
import { PadIconModule } from "pad-ui-lib/icon";

@Component({
  templateUrl: "./button-demo.component.html",
  styleUrls: ["./button-demo.component.scss"],
  standalone: true,
  imports: [NgFor, PadButtonModule, PadIconModule],
})
class ButtonDemoComponent {
  sizes: PadButtonSize[] = ["sm", "md", "lg", "xl", "2xl"];
  colors: PadButtonColor[] = ["primary", "gray", "error", "warning", "success"];
  styles: PadButtonStyle[] = ["default", "outline", "basic"];
  $disabled = signal(false);

  onToggleDisabled() {
    this.$disabled.update(value => !value);
  }
}
export default ButtonDemoComponent;
