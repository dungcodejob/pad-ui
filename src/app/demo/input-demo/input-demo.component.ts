import { JsonPipe } from "@angular/common";
import { Component, OnInit, inject } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from "@angular/forms";
import { PadFormModule } from "pad-ui-lib/form";
import { PadIconModule } from "pad-ui-lib/icon";
import { PadInputModule } from "pad-ui-lib/input-number";

@Component({
  templateUrl: "./input-demo.component.html",
  styleUrls: ["./input-demo.component.scss"],
  standalone: true,
  imports: [
    FormsModule,
    JsonPipe,
    PadFormModule,
    PadInputModule,
    ReactiveFormsModule,
    PadIconModule,
  ],
})
export class InputDemoComponent implements OnInit {
  private readonly _fb = inject(FormBuilder);

  form!: FormGroup<{
    username: FormControl<string>;
    password: FormControl<string>;
  }>;

  price = 10000;

  ngOnInit(): void {
    this.form = this._fb.group({
      username: this._fb.control("", {
        nonNullable: true,
        validators: [Validators.required],
      }),
      password: this._fb.control("", {
        nonNullable: true,
        validators: [Validators.required],
      }),
    });
  }
}

export default InputDemoComponent;
