import { Component, inject } from "@angular/core";
import { FormBuilder, ReactiveFormsModule, Validators } from "@angular/forms";
import { PadFormModule } from "pad-ui-lib/form";
import { PadIconModule } from "pad-ui-lib/icon";
import { PadSelectModule } from "pad-ui-lib/select";

@Component({
  templateUrl: "./select-demo.component.html",
  styleUrls: ["./select-demo.component.scss"],
  standalone: true,
  imports: [PadSelectModule, PadIconModule, ReactiveFormsModule, PadFormModule],
})
export class SelectDemoComponent {
  private readonly _fb = inject(FormBuilder);
  customerControl = this._fb.control("", {
    nonNullable: true,
    validators: [Validators.required],
  });
}

export default SelectDemoComponent;
