import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { PadButtonModule } from "pad-ui-lib/button";
import { PadIconModule } from "pad-ui-lib/icon";

import { OverlayModule } from "@angular/cdk/overlay";
import PadDialogContainerComponent from "./dialog-container.component";
import { PadContentDialogDirective } from "./dialog-content.directive";
import { PadDialogService } from "./dialog.service";
import { PadDialogFooterComponent } from "./ui/dialog-footer.component";
import { PadDialogHeaderComponent } from "./ui/dialog-header.component";
// TODO: Restrictions on the use of 'any' type in the dialog module
@NgModule({
  declarations: [
    PadContentDialogDirective,
    PadDialogContainerComponent,
    PadDialogHeaderComponent,
    PadDialogFooterComponent,
  ],
  providers: [PadDialogService],
  exports: [PadDialogHeaderComponent, PadDialogFooterComponent],
  imports: [CommonModule, OverlayModule, PadButtonModule, PadIconModule],
})
export class PadDialogModule {}
