import { BooleanInput, coerceBooleanProperty } from "@angular/cdk/coercion";
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from "@angular/core";

export type PadDialogFooterLayout = "row" | "column";

@Component({
  selector: "pad-dialog-footer",
  exportAs: "padDialogFooter",
  templateUrl: "./dialog-footer.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./dialog-footer.scss"],
  host: {
    class: "pad-dialog-footer",
    "[class.not-auto-responsive]": "notAutoResponsive",
    "[class.pad-dialog-footer-layout-row]": `layout === 'row'`,
    "[class.pad-dialog-footer-layout-column]": `layout === 'column'`,
  },
})
export class PadDialogFooterComponent {
  @Input() saveText = "Save";
  @Input() layout: PadDialogFooterLayout = "row";
  @Input()
  get notAutoResponsive(): boolean {
    return this._notAutoResponsive;
  }
  set notAutoResponsive(value: BooleanInput) {
    this._notAutoResponsive = coerceBooleanProperty(value);
  }
  private _notAutoResponsive = false;
  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output() close = new EventEmitter<void>();
  @Output() save = new EventEmitter<void>();
}
