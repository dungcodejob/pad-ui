import { BooleanInput, coerceBooleanProperty } from "@angular/cdk/coercion";
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from "@angular/core";

export type PadDialogHeaderLayout = "row" | "column";

@Component({
  selector: "pad-dialog-header",
  exportAs: "padDialogHeader",
  templateUrl: "./dialog-header.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./dialog-header.scss"],
  host: {
    class: "pad-dialog-header",
    "[class.not-auto-responsive]": "notAutoResponsive",
    "[class.pad-dialog-header-layout-row]": `layout === 'row'`,
    "[class.pad-dialog-header-layout-column]": `layout === 'column'`,
  },
})
export class PadDialogHeaderComponent {
  @Input() iconKey!: string;
  @Input({ required: true }) title!: string;
  @Input() subTitle!: string;
  @Input() layout: PadDialogHeaderLayout = "row";
  @Input()
  get notAutoResponsive(): boolean {
    return this._notAutoResponsive;
  }
  set notAutoResponsive(value: BooleanInput) {
    this._notAutoResponsive = coerceBooleanProperty(value);
  }
  private _notAutoResponsive = false;

  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output() close = new EventEmitter<void>();
}
