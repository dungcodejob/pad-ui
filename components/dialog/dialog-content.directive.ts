import { Directive, ViewContainerRef } from "@angular/core";

@Directive({
  selector: "[padDialogContent]",
})
export class PadContentDialogDirective {
  constructor(public viewContainerRef: ViewContainerRef) {} // contentInsertionPoint
}
