import { AnimationEvent } from "@angular/animations";
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ComponentRef,
  EventEmitter,
  HostBinding,
  HostListener,
  OnDestroy,
  Type,
  ViewChild,
  ViewEncapsulation,
} from "@angular/core";

import { PadSafeAny } from "pad-ui-lib/core/type";
import { DialogConfig } from "./dialog-config";
import { PadContentDialogDirective } from "./dialog-content.directive";
import { DialogRef } from "./dialog-ref";
import { AnimationState, fadeAnimation, zoomAnimation } from "./dialog.constants";

@Component({
  selector: "pad-dialog-container",
  templateUrl: "./dialog-container.component.html",
  styleUrls: ["./dialog-container.component.scss"],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [zoomAnimation(), fadeAnimation()],
})
export default class PadDialogContainerComponent<TContentComponent = PadSafeAny>
  implements AfterViewInit, OnDestroy
{
  @ViewChild(PadContentDialogDirective, { static: false })
  contentInsertionPoint!: PadContentDialogDirective;

  animationState = AnimationState.Enter;
  animationStateChanged = new EventEmitter<AnimationEvent>();
  contentComponentType!: Type<TContentComponent>;

  private _contentComponentRef?: ComponentRef<TContentComponent>;

  @HostBinding("@fade") get hostAnimation() {
    // [@fade]
    return {
      value: this.animationState,
      params: {
        timing: this.dialogConfig.containerAnimationTiming,
        delayChild: this.dialogConfig.animationChildDelay,
      },
    };
  }

  @HostListener("@fade.start", ["$event"]) // (@fade.start)="onAnimationStart($event)"
  onAnimationStart(event: AnimationEvent) {
    this.animationStateChanged.emit(event);
  }

  @HostListener("@fade.done", ["$event"])
  onAnimationDone(event: AnimationEvent) {
    this.animationStateChanged.emit(event);
  }

  constructor(
    public readonly dialogConfig: DialogConfig,
    private readonly dialogRef: DialogRef,
    private readonly cdr: ChangeDetectorRef
  ) {}

  ngAfterViewInit(): void {
    this._loadContentComponent();
    this.cdr.detectChanges();
  }

  ngOnDestroy(): void {
    this._contentComponentRef?.destroy();
  }

  startExitAnimation(): void {
    this.animationState = AnimationState.Leave;
    this.cdr.markForCheck();
  }

  onCloseClick() {
    this.closeDialog();
  }

  private closeDialog() {
    this.dialogRef.close();
  }

  private _loadContentComponent(): void {
    if (!this._contentComponentRef) {
      // const factory = this.
      const vcr = this.contentInsertionPoint.viewContainerRef;
      vcr.clear();
      this._contentComponentRef = vcr.createComponent(this.contentComponentType);
    }
  }
}
