import { Overlay, OverlayConfig, OverlayRef } from "@angular/cdk/overlay";
import { ComponentPortal } from "@angular/cdk/portal";
import { Injectable, Injector, Type } from "@angular/core";
import { PadSafeAny } from "pad-ui-lib/core/type";
import { DialogConfig } from "./dialog-config";
import PadDialogContainerComponent from "./dialog-container.component";
import { PAD_DIALOG_DATA } from "./dialog-injector";
import { DialogRef } from "./dialog-ref";

@Injectable()
export class PadDialogService {
  private readonly defaultDialogConfig: DialogConfig;
  constructor(
    private readonly overlay: Overlay,
    private readonly injector: Injector
  ) {
    this.defaultDialogConfig = new DialogConfig();
    this.defaultDialogConfig.overlayConfig = new OverlayConfig({
      disposeOnNavigation: true,
      hasBackdrop: true,
      panelClass: "pad-dialog-panel",
      backdropClass: "pad-dialog-backdrop",
      scrollStrategy: overlay.scrollStrategies.block(),
      positionStrategy: overlay
        .position()
        .global()
        .centerVertically()
        .centerHorizontally(),
    });
  }

  open<TReturnType = PadSafeAny, TContentComponent = PadSafeAny>(
    component: Type<TContentComponent>,
    config?: DialogConfig
  ): DialogRef<TReturnType, TContentComponent> {
    const mergeConfig = this.getMergeConfig(config);
    const overlayRef = this.overlay.create(mergeConfig.overlayConfig);
    const dialogRef = new DialogRef<TReturnType, TContentComponent>(
      overlayRef,
      mergeConfig
    );
    dialogRef.componentInstance = this.attachDialogContainer(
      overlayRef,
      component,
      dialogRef,
      mergeConfig
    );

    return dialogRef;
  }

  private attachDialogContainer<TContentComponent = unknown>(
    overlayRef: OverlayRef,
    component: Type<TContentComponent>,
    dialogRef: DialogRef,
    dialogConfig: DialogConfig
  ) {
    const injector = this.createInjector(dialogRef, dialogConfig);
    const portal = new ComponentPortal(PadDialogContainerComponent, null, injector);
    const containerRef = overlayRef.attach(portal);
    containerRef.instance.contentComponentType = component;
    return containerRef.instance;
  }

  private createInjector(dialogRef: DialogRef, dialogConfig: DialogConfig): Injector {
    return Injector.create({
      providers: [
        {
          provide: PAD_DIALOG_DATA,
          useValue: dialogConfig.data,
        },
        {
          provide: DialogRef,
          useValue: dialogRef,
        },
        {
          provide: DialogConfig,
          useValue: dialogConfig,
        },
      ],
      parent: dialogConfig.injector ?? this.injector,
    });
  }

  private getMergeConfig(config?: Partial<DialogConfig>): DialogConfig {
    if (config == null) {
      return this.defaultDialogConfig;
    }

    return {
      ...this.defaultDialogConfig,
      ...config,
      overlayConfig: {
        ...this.defaultDialogConfig.overlayConfig,
        ...(config?.overlayConfig || {}),
      },
    };
  }
}
