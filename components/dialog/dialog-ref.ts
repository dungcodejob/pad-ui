import { OverlayRef } from "@angular/cdk/overlay";
import { PadSafeAny } from "pad-ui-lib/core/type";
import { filter, map, Observable, race, Subject, take } from "rxjs";
import { DialogConfig } from "./dialog-config";
import PadDialogContainerComponent from "./dialog-container.component";
import { AnimationState } from "./dialog.constants";
const AnimationPhase = {
  START: "start",
  DONE: "done",
};

export class DialogRef<TReturnType = PadSafeAny, TContentComponent = PadSafeAny> {
  private readonly _beforeClosedSubject = new Subject<void>();
  private readonly _afterClosedSubject = new Subject<TReturnType | null>();
  private _result: TReturnType | null = null;
  componentInstance: PadDialogContainerComponent<TContentComponent> | null = null;

  constructor(
    private readonly overlayRef: OverlayRef,
    private readonly config: DialogConfig
  ) {
    if (config.closable) {
      race(
        overlayRef.backdropClick().pipe(map(() => undefined))
        // overlayRef.keydownEvents().pipe(
        //   filter(event => event.code === EventKey.SPACE && !hasModifierKey(event)),
        //   map(() => undefined)
        // )
      )
        .pipe(take(1))
        .subscribe(this.close.bind(this));
    }

    overlayRef.detachments().subscribe(() => {
      this._afterClosedSubject.next(this._result);
      this._afterClosedSubject.complete();
      this.componentInstance = null;
    });
  }

  get beforeClosed$(): Observable<unknown> {
    return this._beforeClosedSubject.asObservable();
  }

  get afterClosed$(): Observable<TReturnType | null> {
    return this._afterClosedSubject.asObservable();
  }

  close(data?: TReturnType) {
    this._result = data ?? null;
    if (this.componentInstance) {
      this.componentInstance.animationStateChanged
        .pipe(
          filter(event => event.phaseName === AnimationPhase.START),
          take(1)
        )
        .subscribe(() => {
          this._beforeClosedSubject.next();
          this._beforeClosedSubject.complete();
          this.overlayRef.detachBackdrop();
        });

      this.componentInstance.animationStateChanged
        .pipe(
          filter(
            event =>
              event.phaseName === AnimationPhase.DONE &&
              event.toState === AnimationState.Leave
          ),
          take(1)
        )
        .subscribe(this.overlayRef.dispose.bind(this.overlayRef));

      this.componentInstance.startExitAnimation();
    } else {
      throw new Error("componentInstance not exist");
    }
  }
}
