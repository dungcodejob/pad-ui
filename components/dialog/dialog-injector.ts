import { InjectionToken } from "@angular/core";
import { PadSafeAny } from "pad-ui-lib/core/type";

export const PAD_DIALOG_DATA = new InjectionToken<PadSafeAny>("PadDialogData");
