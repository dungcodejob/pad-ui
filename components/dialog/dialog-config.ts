import { OverlayConfig } from "@angular/cdk/overlay";
import { Injector, ViewContainerRef } from "@angular/core";
import { PadSafeAny } from "pad-ui-lib/core/type";

export class DialogConfig<TData = PadSafeAny> {
  header = ""; // title of dialog
  closable = true; // determine whether the dialog is closable or not
  containerAnimationTiming = 0.3; // fade
  contentAnimationTiming = 0.2; // zoom
  animationChildDelay = 0;
  data?: TData;
  overlayConfig?: OverlayConfig;
  injector?: Injector;
  viewContainerRef?: ViewContainerRef;
}
