import { EnvironmentProviders, Provider, makeEnvironmentProviders } from "@angular/core";
import { PadRadioButtonComponent } from "./radio-button.component";
import { PadRadioGroupComponent } from "./radio-group.component";
import { PadRadioComponent } from "./radio.component";

export const providePadRadio = (): EnvironmentProviders => {
  const providers: Provider[] = [
    PadRadioGroupComponent,
    PadRadioButtonComponent,
    PadRadioComponent,
  ];

  return makeEnvironmentProviders(providers);
};
