import { Injectable } from "@angular/core";
import { PadSafeAny } from "pad-ui-lib/core/type";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class PadRadioService {
  private readonly _selectedSubject = new BehaviorSubject<PadSafeAny>(null);
  name: string | null = null;
  get selected$() {
    return this._selectedSubject.asObservable();
  }

  get selected() {
    return this._selectedSubject.value;
  }

  select(value: PadSafeAny) {
    if (this._selectedSubject.value != value) {
      this._selectedSubject.next(value);
    }
  }
}
