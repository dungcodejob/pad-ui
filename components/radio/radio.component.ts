import { A11yModule, FocusMonitor } from "@angular/cdk/a11y";
import { BooleanInput, coerceBooleanProperty } from "@angular/cdk/coercion";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  DestroyRef,
  ElementRef,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation,
  forwardRef,
  inject,
} from "@angular/core";
import { takeUntilDestroyed } from "@angular/core/rxjs-interop";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
import { fromEvent } from "rxjs";

import { OnChangeType, OnTouchedType, PadSafeAny } from "pad-ui-lib/core/type";
import { PadRadioGroupComponent } from "./radio-group.component";
import { PadRadioService } from "./radio.service";

export type PadRadioSize = "sm" | "md";
let nextUniqueId = 0;
@Component({
  selector: "pad-radio",
  exportAs: "padRadio",
  template: `
    <span
      [class.checked]="checked"
      [class.disabled]="disabled"
      class="pad-radio__outer-circle">
      <div class="pad-radio__inner-circle"></div>
      <input #inputElement type="radio" [attr.name]="name" [checked]="checked" />
    </span>
    <span> <ng-content></ng-content></span>
  `,
  styleUrls: ["./radio.scss"],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [PadRadioGroupComponent, A11yModule],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PadRadioComponent),
      multi: true,
    },
  ],
  host: {
    class: "pad-radio",
    "[class.pad-radio-size-sm]": `padSize === 'sm'`,
    "[class.pad-radio-size-md]": `padSize === 'md'`,
    "[class.disabled]": "disabled",
    "[class.checked]": "checked",
  },
})
export class PadRadioComponent implements ControlValueAccessor, OnInit, OnDestroy {
  protected readonly _cdr = inject(ChangeDetectorRef);
  protected readonly _destroyRef = inject(DestroyRef);
  protected readonly _radioService = inject(PadRadioService, {
    optional: true,
  });
  protected readonly _ngZone = inject(NgZone);
  protected readonly _focusMonitor = inject(FocusMonitor);
  protected readonly _elementRef = inject(ElementRef);

  @ViewChild("inputElement", { static: true })
  inputElement!: ElementRef<HTMLInputElement>;

  @Input() padSize: PadRadioSize = "sm";
  @Input() padValue: PadSafeAny | null = null;
  @Input() id = `pad-radio-${++nextUniqueId}`;
  @Input()
  get checked(): boolean {
    return this._checked;
  }
  set checked(value: BooleanInput) {
    const newValue = coerceBooleanProperty(value);
    if (newValue !== this._checked) {
      this._checked = newValue;
      this._radioService?.select(this.padValue);
      this.markForCheck();
    }
  }
  private _checked = false;

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: BooleanInput) {
    const newValue = coerceBooleanProperty(value);

    if (newValue !== this.disabled) {
      this._disabled = newValue;
      this.markForCheck();
    }
  }
  private _disabled = false;
  private _isNgModel = false;

  private _onChange!: OnChangeType;
  private _onTouched!: OnTouchedType;

  name: string | null = null;

  writeValue(obj: PadSafeAny): void {
    this._checked = obj;
  }
  registerOnChange(fn: PadSafeAny): void {
    this._isNgModel = true;
    this._onChange = fn;
  }
  registerOnTouched(fn: PadSafeAny): void {
    this._onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this._disabled = isDisabled;
  }

  ngOnInit() {
    if (this._radioService) {
      this.name = this._radioService.name;
      this.markForCheck();
      this._radioService.selected$
        .pipe(takeUntilDestroyed(this._destroyRef))
        .subscribe(newSelected => {
          const isSelected = newSelected === this.padValue;

          if (this._isNgModel && this._checked !== isSelected) {
            this._onChange(false);
          }
          this._checked = isSelected;
          this.markForCheck();
        });
    }

    this._setupClickListener();
  }
  ngOnDestroy(): void {
    this._focusMonitor.stopMonitoring(this._elementRef);
  }

  markForCheck() {
    console.log("markForCheck");
    this._cdr.markForCheck();
  }

  focus(): void {
    this._focusMonitor.focusVia(this.inputElement, "keyboard");
    if (this._onTouched) {
      this._onTouched();
    }
  }

  private _setupClickListener() {
    this._ngZone.runOutsideAngular(() => {
      fromEvent<MouseEvent>(this._elementRef.nativeElement, "click")
        .pipe(takeUntilDestroyed(this._destroyRef))
        .subscribe((event: MouseEvent) => {
          /** prevent label click triggered twice. **/
          event.stopPropagation();
          event.preventDefault();
          if (this._disabled || this._checked) {
            return;
          }

          this._ngZone.run(() => {
            this.focus();
            this._radioService?.select(this.padValue);
            if (this._isNgModel) {
              this._checked = true;
              this._onChange(true);
            }
            this.markForCheck();
          });
        });
    });
  }
}
