import { BooleanInput, coerceBooleanProperty } from "@angular/cdk/coercion";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  DestroyRef,
  Input,
  OnInit,
  QueryList,
  ViewEncapsulation,
  forwardRef,
  inject,
} from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";

import { takeUntilDestroyed } from "@angular/core/rxjs-interop";
import { OnChangeType, OnTouchedType, PadSafeAny } from "pad-ui-lib/core/type";
import { PadRadioComponent, PadRadioSize } from "./radio.component";
import { PadRadioService } from "./radio.service";

@Component({
  selector: "pad-radio-group",
  exportAs: "padRadioGroup",
  template: `<ng-content></ng-content>`,
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  providers: [
    PadRadioService,
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PadRadioGroupComponent),
      multi: true,
    },
  ],
  host: {
    class: "pad-radio-group",
    "[class.pad-radio-group-size-sm]": `padSize === 'sm'`,
    "[class.pad-radio-group-size-md]": `padSize === 'md'`,
    "[class.disabled]": "disabled",
  },
})
export class PadRadioGroupComponent implements ControlValueAccessor, OnInit {
  protected readonly _cdr = inject(ChangeDetectorRef);
  protected readonly _destroyRef = inject(DestroyRef);
  protected readonly _radioService = inject(PadRadioService);
  @Input()
  get value() {
    return this._radioService.selected;
  }
  set value(selected: PadSafeAny) {
    this._radioService.select(selected);
  }
  // private readonly _selected$ = new BehaviorSubject<PadSafeAny>(1);

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: BooleanInput) {
    const newValue = coerceBooleanProperty(value);

    if (newValue !== this.disabled) {
      this._disabled = newValue;
      this._cdr.markForCheck();
    }
  }
  private _disabled = false;

  @Input() padSize: PadRadioSize = "md";
  @Input() name: string | null = null;
  private _onChange!: OnChangeType;
  private _onTouched!: OnTouchedType;

  @ContentChildren(forwardRef(() => PadRadioComponent), { descendants: true })
  _radios!: QueryList<PadRadioComponent>;

  writeValue(obj: PadSafeAny): void {
    this._radioService.select(obj);
  }
  registerOnChange(fn: PadSafeAny): void {
    this._onChange = fn;
  }
  registerOnTouched(fn: PadSafeAny): void {
    this._onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this._disabled = isDisabled;
  }

  touch() {
    if (this._onTouched) {
      this._onTouched();
    }
  }

  ngOnInit() {
    this._radioService.name = this.name;
    this._radioService.selected$
      .pipe(takeUntilDestroyed(this._destroyRef))
      .subscribe(value => {
        if (this._onChange) {
          this._onChange(value);
        }
      });
    // this._selected$
    //   .pipe(takeUntilDestroyed(this.destroyRef))
    //   .subscribe((value) => {
    //     if (this._selected$.value != value) {
    //       this._onChange(value);
    //     };
    //   });
  }

  private _markRadiosForCheck() {
    if (this._radios) {
      this._radios.forEach(radio => radio.markForCheck());
    }
  }
}
