import { NgModule } from "@angular/core";
import { PadRadioButtonComponent } from "./radio-button.component";
import { PadRadioGroupComponent } from "./radio-group.component";
import { PadRadioComponent } from "./radio.component";

@NgModule({
  imports: [PadRadioComponent, PadRadioButtonComponent, PadRadioGroupComponent],
  providers: [],
  exports: [PadRadioComponent, PadRadioButtonComponent, PadRadioGroupComponent],
})
export class PadRadioModule {}
