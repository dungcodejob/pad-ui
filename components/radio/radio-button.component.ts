import { ChangeDetectionStrategy, Component, ViewEncapsulation } from "@angular/core";
import { PadRadioComponent } from "./radio.component";

@Component({
  selector: "pad-radio-button",
  template: `
    <span
      [class.checked]="checked"
      [class.disabled]="disabled"
      class="pad-radio__outer-circle">
      <div class="pad-radio__inner-circle"></div>
      <input #inputElement type="radio" [attr.name]="name" [checked]="checked" />
    </span>

    <div>
      <ng-content></ng-content>
    </div>
  `,
  styleUrls: ["./radio-button.scss"],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  host: {
    class: "pad-radio-button",
    "[class.pad-radio-size-sm]": `padSize === 'sm'`,
    "[class.pad-radio-size-md]": `padSize === 'md'`,
    "[class.disabled]": "disabled",
    "[class.checked]": "checked",
  },
  providers: [
    {
      provide: PadRadioComponent,
      useExisting: PadRadioButtonComponent,
    },
  ],
})
export class PadRadioButtonComponent extends PadRadioComponent {}
