import { Directive, ElementRef, HostListener, Input, forwardRef } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
import { OnTouchedType, PadSafeAny } from "pad-ui-lib/core/type";

export type PadInputSize = "lg" | "md";

@Directive({
  selector: "input[padInputNumber]",
  exportAs: "padInputNumber",
  host: {
    class: "pad-input",
    "[class.pad-input-disabled]": "disabled",
    "[disabled]": "disabled",
    "[class.pad-input-size-md]": `padSize === 'md'`,
    "[class.pad-input-size-lg]": `padSize === 'lg'`,
  },
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PadInputNumberDirective),
      multi: true,
    },
  ],
})
export class PadInputNumberDirective implements ControlValueAccessor {
  @Input() decimalSeparator = ".";
  @Input() thousandSeparator = ",";
  @Input() maximumFractionDigits = 0;

  // @Input('value')
  // set value(value: string | number | null) {
  //   this.onInputChange(value ? value.toString() : '');
  // }

  #numberValue: string | null = null;
  #formatValue = "";
  private _onChange!: (value: number | null) => void;
  private _onTouched!: OnTouchedType;

  @HostListener("keydown", ["$event"])
  onInputKeyDown(event: KeyboardEvent) {
    if (event.key == "Backspace") {
      const prevValue = (event.target as HTMLInputElement).value;
      const position = this.element.nativeElement.selectionStart;
      if (position) {
        const character = prevValue[position - 1];
        if (character === this.thousandSeparator || character === this.decimalSeparator) {
          event.preventDefault();
          this.element.nativeElement.setSelectionRange(position - 1, position - 1);
        }
      }
    }

    if (event.key == "Delete") {
      const prevValue = (event.target as HTMLInputElement).value;
      const position = this.element.nativeElement.selectionStart;
      if (position) {
        const character = prevValue[position];
        if (character === this.thousandSeparator || character === this.decimalSeparator) {
          event.preventDefault();
          this.element.nativeElement.setSelectionRange(position + 1, position + 1);
        }
      }
    }
  }

  @HostListener("input", ["$event.target.value"])
  onInputChange(inputValue: string) {
    if (inputValue) {
      const position = this.element.nativeElement.selectionStart;
      const num = this.unFormatValue(inputValue);
      const displayText = this.formatMoney(num);
      this.element.nativeElement.value = displayText;
      this._onChange(num);

      const [integer, decimal] = displayText.split(this.decimalSeparator);
      console.log(decimal);
      if (position) {
        if (position < integer.length) {
          const newPosition =
            position - (inputValue.length - this.element.nativeElement.value.length);
          this.element.nativeElement.setSelectionRange(newPosition, newPosition);
        } else {
          this.element.nativeElement.setSelectionRange(position, position);
        }
      }
    } else {
      this.element.nativeElement.value = "";
      this._onChange(null);
    }
  }

  constructor(private readonly element: ElementRef<HTMLInputElement>) {}

  writeValue(obj: PadSafeAny): void {
    this.element.nativeElement.value = obj ? this.formatMoney(obj) : "";
    this.#numberValue = obj;
  }
  registerOnChange(fn: PadSafeAny): void {
    this._onChange = fn;
  }
  registerOnTouched(fn: PadSafeAny): void {
    this._onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    console.log("isDisabled", isDisabled);
  }

  formatMoney(value: number) {
    const prefix = value < 0 ? "-" : "";
    const temp = Math.abs(value).toString();

    const amount = this.maximumFractionDigits ? parseFloat(temp) : parseInt(temp);

    const i = parseInt(
      Math.abs(Number(amount) ?? 0).toFixed(this.maximumFractionDigits)
    ).toString();
    const j = i.length > 3 ? i.length % 3 : 0;

    // if (isNaN(Number(amount))) {
    //   debugger;
    // }

    return (
      prefix +
      (j ? i.substring(0, j) + this.thousandSeparator : "") +
      i.substring(j).replace(/(\d{3})(?=\d)/g, "$1" + this.thousandSeparator) +
      (this.maximumFractionDigits
        ? this.decimalSeparator +
          Math.abs(amount - Number(i))
            .toFixed(this.maximumFractionDigits)
            .slice(2)
        : "")
    );
  }

  unFormatValue(value: string): number {
    const temp = value.replace(this.decimalSeparator, ".").replace(/[^\d.-]/g, "");

    return Number(temp);
  }

  #isLastCharacterDecimalSeparator(value: string) {
    const num = parseInt(value[value.length - 1]);
    return isNaN(num);
  }
}
