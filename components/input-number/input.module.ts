import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { PadInputNumberDirective } from "./input-number.directive";

@NgModule({
  declarations: [PadInputNumberDirective],
  exports: [PadInputNumberDirective],
  imports: [CommonModule],
})
export class PadInputModule {}
