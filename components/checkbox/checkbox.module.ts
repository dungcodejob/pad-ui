import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { PadCheckboxComponent } from "./checkbox.component";

@NgModule({
  declarations: [PadCheckboxComponent],
  exports: [PadCheckboxComponent],
  imports: [CommonModule, FormsModule],
})
export class PadCheckboxModule {}
