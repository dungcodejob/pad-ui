import { BooleanInput, coerceBooleanProperty } from "@angular/cdk/coercion";
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation,
  forwardRef,
  inject,
} from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
import { OnChangeType, OnTouchedType, PadSafeAny } from "pad-ui-lib/core/type";
import { Subject, fromEvent, takeUntil } from "rxjs";

let nextUniqueId = 0;
export type PadCheckboxSize = "sm" | "md";
@Component({
  selector: "pad-checkbox",
  exportAs: "padCheckbox",
  templateUrl: "./checkbox.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./checkbox.scss"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PadCheckboxComponent),
      multi: true,
    },
  ],
  host: {
    class: "pad-checkbox",
    "[class.pad-checkbox-size-sm]": `padSize === 'sm'`,
    "[class.pad-checkbox-size-md]": `padSize === 'md'`,
    "[class.disabled]": "disabled",
  },
})
export class PadCheckboxComponent implements OnInit, OnDestroy, ControlValueAccessor {
  // TODO: typing for checkbox value
  protected readonly cdr = inject(ChangeDetectorRef);
  protected readonly ngZone = inject(NgZone);

  @ViewChild("input", { static: true }) input!: ElementRef<HTMLInputElement>;
  @Input() id = `pad-checkbox-${++nextUniqueId}`;
  @Input() name: string | null = null;
  @Input() padSize: PadCheckboxSize = "md";
  @Input()
  get checked(): boolean {
    return this._checked;
  }
  set checked(value: BooleanInput) {
    const checked = coerceBooleanProperty(value);
    if (checked != this.checked) {
      this._checked = checked;
      this.cdr.markForCheck();
    }
  }
  private _checked = false;

  @Input()
  get required(): boolean {
    return this._required;
  }
  set required(value: BooleanInput) {
    this._required = coerceBooleanProperty(value);
  }
  private _required!: boolean;

  @Input()
  get disabled(): boolean {
    return this._disabled;
  }
  set disabled(value: BooleanInput) {
    const newValue = coerceBooleanProperty(value);

    if (newValue !== this.disabled) {
      this._disabled = newValue;
      this.cdr.markForCheck();
    }
  }
  private _disabled = false;
  // eslint-disable-next-line @angular-eslint/no-output-native
  @Output() readonly change = new EventEmitter<boolean>();

  private _destroy$ = new Subject<void>();

  ngOnInit(): void {
    this.ngZone.runOutsideAngular(() => {
      fromEvent(this.input.nativeElement, "click")
        .pipe(takeUntil(this._destroy$))
        .subscribe(event => {
          if (this.disabled) {
            event.preventDefault();
            return;
          }
          this.ngZone.run(() => {
            this.onChange(!this.checked);
            this.cdr.markForCheck();
          });
        });
      fromEvent(this.input.nativeElement, "click")
        .pipe(takeUntil(this._destroy$))
        .subscribe(event => event.stopPropagation());
    });
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.subscribe();
  }

  private _onChanged!: OnChangeType;
  private _onTouched!: OnTouchedType;

  writeValue(obj: PadSafeAny): void {
    this._checked = !!obj;
  }
  registerOnChange(fn: PadSafeAny): void {
    this._onChanged = fn;
  }
  registerOnTouched(fn: PadSafeAny): void {
    this._onTouched = fn;
  }

  toggle() {
    this.checked = !this.checked;
    this._onChanged(this.checked);
  }

  onChange(value: boolean) {
    if (!this.disabled) {
      this.checked = value;
      this._onChanged(value);
      this.change.next(value);
    }
  }
}
