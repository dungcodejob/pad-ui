import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ViewEncapsulation,
} from "@angular/core";
import { PadIconComponent } from "pad-ui-lib/icon";
import { PAD_BUTTON_HOST, PadBaseButtonComponent } from "./base-button.component";

@Component({
  selector: "button[pad-icon-button],a[pad-icon-button]",
  exportAs: "padButton",
  template: `<ng-content></ng-content>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./icon-button.scss"],
  host: PAD_BUTTON_HOST,
})
export class PadIconButtonComponent extends PadBaseButtonComponent {
  @ContentChild(PadIconComponent) icon?: PadIconComponent;
}
