import { ChangeDetectionStrategy, Component, ViewEncapsulation } from "@angular/core";
import { PAD_BUTTON_HOST, PadBaseButtonComponent } from "./base-button.component";

export type PadButtonStyle = "outline" | "basic" | "default";
export type PadButtonSize = "sm" | "md" | "lg" | "xl" | "2xl";
export type PadButtonColor = "primary" | "gray" | "error" | "warning" | "success";

@Component({
  selector: "button[pad-button],a[pad-button]",
  exportAs: "padButton",
  template: `<ng-content></ng-content>`,
  styleUrls: ["./button.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: PAD_BUTTON_HOST,
})
export class PadButtonComponent extends PadBaseButtonComponent {}
