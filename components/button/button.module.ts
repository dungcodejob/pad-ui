import { NgModule } from "@angular/core";
import { PadButtonComponent } from "./button.component";
import { PadIconButtonComponent } from "./icon-button.component";

@NgModule({
  declarations: [PadButtonComponent, PadIconButtonComponent],
  exports: [PadButtonComponent, PadIconButtonComponent],
  imports: [],
})
export class PadButtonModule {}
