import { Directive, Input, booleanAttribute } from "@angular/core";

export type PadButtonStyle = "outline" | "basic" | "default";
export type PadButtonSize = "sm" | "md" | "lg" | "xl" | "2xl";
export type PadButtonColor = "primary" | "gray" | "error" | "warning" | "success";

export const PAD_BUTTON_HOST = {
  class: "pad-button",
  "[class.pad-button-style-default]": `padStyle === 'default'`,
  "[class.pad-button-style-outline]": `padStyle === 'outline'`,
  "[class.pad-button-style-basic]": `padStyle === 'basic'`,
  "[class.pad-button-size-sm]": `padSize === 'sm'`,
  "[class.pad-button-size-md]": `padSize === 'md'`,
  "[class.pad-button-size-lg]": `padSize === 'lg'`,
  "[class.pad-button-size-xl]": `padSize === 'xl'`,
  "[class.pad-button-size-2xl]": `padSize === '2xl'`,
  "[class.pad-button-color-primary]": `padColor === 'primary'`,
  "[class.pad-button-color-gray]": `padColor === 'gray'`,
  "[class.pad-button-color-error]": `padColor === 'error'`,
  "[class.pad-button-color-warning]": `padColor === 'warning'`,
  "[class.pad-button-color-success]": `padColor === 'success'`,
  "[disabled]": "disabled",
};

@Directive()
export abstract class PadBaseButtonComponent {
  // @Input({ transform: booleanAttribute }) disabled: boolean = false;
  @Input() padColor: PadButtonColor = "primary";
  @Input() padStyle: PadButtonStyle = "default";
  @Input() padSize: PadButtonSize = "lg";
  @Input({ transform: booleanAttribute }) disabled = false;
}
