import { AfterViewInit } from "@angular/core";
// Angular
import { NgFor, NgTemplateOutlet } from "@angular/common";
import {
  AfterContentInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  EventEmitter,
  Input,
  NgZone,
  OnDestroy,
  Output,
  QueryList,
  TemplateRef,
  ViewChild,
  ViewChildren,
  ViewEncapsulation,
  booleanAttribute,
  inject,
} from "@angular/core";

// Rx
import {
  Observable,
  Subscription,
  defer,
  filter,
  merge,
  startWith,
  switchMap,
  take,
} from "rxjs";

// Third parties
import { CompareWith, PadSafeAny } from "pad-ui-lib/core/type";
import { PadMenuModule } from "pad-ui-lib/menu";

// Project
import { PadOptionComponent, PadOptionSelectionChange } from "./option.component";

export interface AutocompleteDataSourceItem {
  value: string;
  label: string;
}

export type AutocompleteDataSource = Array<AutocompleteDataSourceItem | string | number>;

@Component({
  selector: "pad-autocomplete",
  exportAs: "padAutocomplete",
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  template: `<ng-template>
    <pad-menu style="width: 100%">
      <ng-template
        *ngTemplateOutlet="data ? optionsTemplate : contentTemplate"></ng-template>

      <ng-template #contentTemplate>
        <ng-content></ng-content>
      </ng-template>

      <ng-template #optionsTemplate>
        <pad-option
          *ngFor="let item of data"
          [value]="item"
          [label]="getOptionDisplay(item)">
          {{ getOptionDisplay(item) }}
        </pad-option>
      </ng-template>
    </pad-menu>
  </ng-template>`,
  standalone: true,
  imports: [NgFor, NgTemplateOutlet, PadMenuModule, PadOptionComponent],
})
export class PadAutocompleteComponent
  implements AfterContentInit, AfterViewInit, OnDestroy
{
  private readonly _cdr = inject(ChangeDetectorRef);
  private readonly _zone = inject(NgZone);

  @Input() padWidth?: string | number;
  @Input() compareWith: CompareWith = (o1, o2) => o1 === o2;
  @Input() data?: AutocompleteDataSource;
  @Input({ transform: booleanAttribute }) autoActiveFirstOption: boolean = false;
  @Input({ transform: booleanAttribute }) backfill: boolean = false;

  // eslint-disable-next-line @angular-eslint/no-output-on-prefix
  @Output() readonly onSelectedChange = new EventEmitter<PadOptionComponent>();
  @Output() readonly opened = new EventEmitter<boolean>();

  private _activeOptionIndex: number | null = null;
  private _selectionChangeSubscription: Subscription = Subscription.EMPTY;
  // private _optionMouseEnterSubscription: Subscription = Subscription.EMPTY;
  // private _dataSourceChangeSubscription: Subscription = Subscription.EMPTY;
  /** Provided by content */
  @ContentChildren(PadOptionComponent, { descendants: true })
  contentOptions!: QueryList<PadOptionComponent>;
  /** Provided by dataSource */
  @ViewChildren(PadOptionComponent)
  dataSourceOptions!: QueryList<PadOptionComponent>;
  @ViewChild(TemplateRef, { static: false }) template?: TemplateRef<PadSafeAny>;

  isOpen: boolean = false;
  showPanel: boolean = true;
  activeOption: PadOptionComponent | null = null;

  get options(): QueryList<PadOptionComponent> {
    // first dataSource
    if (this.data) {
      return this.dataSourceOptions;
    } else {
      return this.contentOptions;
    }
  }

  readonly optionSelections: Observable<PadOptionSelectionChange> = defer(() => {
    if (this.options) {
      return this.options.changes.pipe(
        startWith(this.options),
        switchMap(() => merge(...this.options.map(option => option.onSelectionChange)))
      );
    }

    // If there are any subscribers before `ngAfterViewInit`, the `autocomplete` will be undefined.
    // Return a stream that we'll replace with the real one once everything is in place.
    return this._zone.onStable.pipe(
      take(1),
      switchMap(() => this.optionSelections)
    );
  });

  ngAfterContentInit(): void {
    if (!this.data) {
      this._initialize();
    }
  }

  ngAfterViewInit(): void {
    if (this.data) {
      this._initialize();
    }
  }

  ngOnDestroy(): void {
    this._selectionChangeSubscription.unsubscribe();
  }

  setVisibility(): void {
    this.showPanel = !!this.options.length;
    this._cdr.markForCheck();
  }

  activeNextOption(): void {
    const nextIndex =
      this._activeOptionIndex !== null &&
      this._activeOptionIndex < this.options.length - 1
        ? this._activeOptionIndex + 1
        : 0;

    this.setActiveOption(nextIndex);
  }

  activePreviousOption(): void {
    const previousIndex =
      this._activeOptionIndex !== null && this._activeOptionIndex - 1 >= 0
        ? this._activeOptionIndex - 1
        : this.options.length - 1;

    this.setActiveOption(previousIndex);
  }

  getOptionDisplay(value: AutocompleteDataSourceItem | string | number) {
    if (typeof value === "object") {
      return value.label;
    }

    return value.toString();
  }

  getOptionIndex(value: PadSafeAny) {
    return this.options.reduce(
      (prevIndex, option, index) =>
        prevIndex === -1
          ? this.compareWith(value, option.value)
            ? index
            : -1
          : prevIndex,
      -1
    );
  }

  getOption(value: PadSafeAny): PadOptionComponent | null {
    return this.options.find(option => this.compareWith(value, option.value)) || null;
  }

  setActiveOption(index: number): void {
    const option = this.options.get(index);
    if (option && !option.activated) {
      this.activeOption = option;
      this._activeOptionIndex = index;
      this.clear(option);
      option.active();
    } else {
      this.activeOption = null;
      this._activeOptionIndex = -1;
      this.clear();
    }

    this._cdr.markForCheck();
  }

  clear(skip: PadOptionComponent | null = null, deselect: boolean = false): void {
    this.options.forEach(option => {
      if (option !== skip) {
        if (deselect) {
          option.deselect();
        }
        option.inactive();
      }
    });
  }

  private _initialize() {
    this._subscribeOptionChanges();
  }

  private _subscribeOptionChanges(): void {
    this._selectionChangeSubscription.unsubscribe();

    this._selectionChangeSubscription = this.optionSelections
      .pipe(filter(event => event.isUserInput))
      .subscribe(event => {
        console.log("autocomplete option changes emit");

        // event.source.select();
        // event.source.active();
        // this.activeOption = event.source;
        // this._activeOptionIndex = this.getOptionIndex(this.activeOption.value);
        // this.clear(event.source, true);
        this.onSelectedChange.emit(event.source);
      });
  }
}
