// Angular
import { NgModule } from "@angular/core";

// Third parties
import { PadAutocompleteTriggerDirective } from "./autocomplete-trigger.directive";
import { PadAutocompleteComponent } from "./autocomplete.component";
import { PadOptionComponent } from "./option.component";

@NgModule({
  declarations: [PadAutocompleteTriggerDirective],
  exports: [
    PadAutocompleteTriggerDirective,
    PadAutocompleteComponent,
    PadOptionComponent,
  ],
  imports: [PadAutocompleteComponent, PadOptionComponent],
})
export class PadAutocompleteModule {}
