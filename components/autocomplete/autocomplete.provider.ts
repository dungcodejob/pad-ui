import { EnvironmentProviders, Provider, makeEnvironmentProviders } from "@angular/core";
import { PadAutocompleteTriggerDirective } from "./autocomplete-trigger.directive";
import { PadAutocompleteComponent } from "./autocomplete.component";
import { PadOptionComponent } from "./option.component";
export const providePadAutocomplete = (): EnvironmentProviders => {
  const providers: Provider[] = [
    PadAutocompleteTriggerDirective,
    PadAutocompleteComponent,
    PadOptionComponent,
  ];

  return makeEnvironmentProviders(providers);
};
