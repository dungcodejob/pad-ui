// Angular
import { DOCUMENT } from "@angular/common";
import {
  DestroyRef,
  Directive,
  ElementRef,
  ExistingProvider,
  HostListener,
  Input,
  OnDestroy,
  ViewContainerRef,
  forwardRef,
  inject,
} from "@angular/core";
import { takeUntilDestroyed } from "@angular/core/rxjs-interop";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";

// Cdk
import { DOWN_ARROW, ENTER, ESCAPE, TAB, UP_ARROW } from "@angular/cdk/keycodes";
import {
  FlexibleConnectedPositionStrategy,
  Overlay,
  OverlayConfig,
  OverlayRef,
  PositionStrategy,
} from "@angular/cdk/overlay";
import { TemplatePortal } from "@angular/cdk/portal";

// Rx
import { Subscription, filter, tap } from "rxjs";

// Third parties
import { OnChangeType, OnTouchedType, PadSafeAny } from "pad-ui-lib/core/type";

// Project
import { PadAutocompleteComponent } from "./autocomplete.component";
import { POSITION_MAP } from "./connection-position-pair";

export function getPadAutocompleteMissingPanelError(): Error {
  return Error(
    "Attempting to open an undefined instance of `pad-autocomplete`. " +
      "Make sure that the id passed to the `padAutocomplete` is correct and that " +
      "you're attempting to open it after the ngAfterContentInit hook."
  );
}

export const PAD_AUTOCOMPLETE_VALUE_ACCESSOR: ExistingProvider = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => PadAutocompleteTriggerDirective),
  multi: true,
};

@Directive({
  selector: "input[padAutocomplete], textarea[padAutocomplete]",
  exportAs: "padAutocompleteTrigger",
  providers: [PAD_AUTOCOMPLETE_VALUE_ACCESSOR],
  host: {
    autocomplete: "off",
    "aria-autocomplete": "list",
    "(focusin)": "onHandleFocus()",
    "(blur)": "onHandleBlur()",
  },
})
export class PadAutocompleteTriggerDirective implements ControlValueAccessor, OnDestroy {
  private readonly _elementRef = inject(ElementRef);
  private readonly _overlay = inject(Overlay);
  private readonly _vcr = inject(ViewContainerRef);
  private readonly _document = inject(DOCUMENT, { optional: true });
  private readonly _destroyRef = inject(DestroyRef);


  @Input({ required: true }) padAutocomplete!: PadAutocompleteComponent;

  private _overlayRef: OverlayRef | null = null;
  private _portal: TemplatePortal | null = null;
  private _positionStrategy!: FlexibleConnectedPositionStrategy;
  private _previousValue: string | number | null = null;
  private _previousOptionValue: PadSafeAny | null = null;
  private _selectionChangeSubscription: Subscription = Subscription.EMPTY;
  private _overlayOutsideClickSubscription: Subscription = Subscription.EMPTY;
  private _optionsChangeSubscription: Subscription = Subscription.EMPTY;

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  onChange: OnChangeType = () => {};
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  onTouched: OnTouchedType = () => {};
  panelOpen = true;

  get activeOption() {
    if (this.padAutocomplete && this.padAutocomplete.options.length) {
      return this.padAutocomplete.activeOption;
    } else {
      return null;
    }
  }

  ngOnDestroy(): void {
    if (this._overlayRef) {
      this.closePanel();
    }
  }

  writeValue(obj: PadSafeAny): void {
    Promise.resolve(null).then(() => this._setTriggerValue(obj));
  }
  registerOnChange(fn: PadSafeAny): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: PadSafeAny): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    // TODO: handle disabled state change
    console.log("isDisabled", isDisabled);
  }

  onHandleFocus() {
    if (this._canOpen()) {
      this.openPanel();
    }
  }

  onHandleBlur(): void {
    this.onTouched();
  }

  @HostListener("keydown", ["$event"])
  onHandleKeyDown(event: KeyboardEvent): void {
    const keyCode = event.keyCode;
    const isArrowKey = keyCode === UP_ARROW || keyCode === DOWN_ARROW;
    if (keyCode === ESCAPE) {
      event.preventDefault();
    }

    if (this.panelOpen && (keyCode === ESCAPE || keyCode === TAB)) {
      // Reset value when tab / ESC close
      if (this.activeOption && this.activeOption.displayText !== this._previousValue) {
        this._setTriggerValue(this._previousValue);
      }
      this.closePanel();
    } else if (this.panelOpen && keyCode === ENTER) {
      if (this.padAutocomplete.showPanel) {
        event.preventDefault();
        if (this.activeOption) {
          this.activeOption.selectViaInteraction();
        } else {
          this.closePanel();
        }
      }
    } else if (this.panelOpen && isArrowKey && this.padAutocomplete.showPanel) {
      event.stopPropagation();
      event.preventDefault();
      if (keyCode === UP_ARROW) {
        this.padAutocomplete.activePreviousOption();
      } else {
        this.padAutocomplete.activeNextOption();
      }

      if (this.activeOption) {
        this.activeOption.scrollIntoView();
      }
      this._doBackfill();
    }
  }

  @HostListener("input", ["$event"])
  onHandleInput(event: KeyboardEvent): void {
    const target = event.target as HTMLInputElement;
    let value: number | string | null = target.value;

    if (target.type === "number") {
      value = value === "" ? null : parseFloat(value);
    }

    if (this._previousValue !== value) {
      this._previousValue = value;
      this.onChange(value);

      if (this._canOpen() && this._document?.activeElement === event.target) {
        this.openPanel();
      }
    }
  }

  openPanel(): void {
    this._previousValue = this._elementRef.nativeElement.value;
    this._attachOverlay();
    this._updateStatus();
  }

  closePanel(): void {
    if (!this.panelOpen) {
      return;
    }

    this.padAutocomplete.isOpen = false;
    this.padAutocomplete.opened.emit(false);
    this.panelOpen = false;

    if (!this._overlayRef || !this._overlayRef.hasAttached()) {
      return;
    }

    this._overlayRef.detach();
    this._selectionChangeSubscription.unsubscribe();
    this._overlayOutsideClickSubscription.unsubscribe();
    this._optionsChangeSubscription.unsubscribe();
    this._portal = null;
  }

  private _attachOverlay(): void {
    if (!this.padAutocomplete) {
      throw getPadAutocompleteMissingPanelError();
    }

    if (!this._portal && this.padAutocomplete.template) {
      this._portal = new TemplatePortal(this.padAutocomplete.template, this._vcr);
    }

    if (!this._overlayRef) {
      this._overlayRef = this._overlay.create(this._getOverlayConfig());
    }

    if (this._overlayRef && !this._overlayRef.hasAttached()) {
      this._overlayRef.attach(this._portal);

      this._subscribeSelectionChange();
      this._subscribeOptionsChange();
      this._subscribeOverlayOutsideClick();
      this._overlayRef
        .detachments()
        .pipe(takeUntilDestroyed(this._destroyRef))
        .subscribe(() => {
          this.closePanel();
        });
    }

    this.padAutocomplete.isOpen = true;
    this.padAutocomplete.opened.next(true);
    this.panelOpen = true;
  }

  private _getOverlayConfig(): OverlayConfig {
    return new OverlayConfig({
      positionStrategy: this._getOverlayPosition(),
      scrollStrategy: this._overlay.scrollStrategies.reposition(),
      width: this.padAutocomplete.padWidth ?? this._getHostWidth(),
      // panelClass: this._defaults?.overlayPanelClass,
    });
  }

  private _getOverlayPosition(): PositionStrategy {
    const position = [POSITION_MAP.bottomLeft, POSITION_MAP.topLeft];
    this._positionStrategy = this._overlay
      .position()
      .flexibleConnectedTo(this._getConnectedElement())
      .withFlexibleDimensions(false)
      .withPush(false)
      .withPositions(position);

    return this._positionStrategy;
  }

  private _updateStatus(): void {
    if (this._overlayRef) {
      this._overlayRef.updateSize({
        width: this.padAutocomplete.padWidth || this._getHostWidth(),
      });
    }
    this.padAutocomplete.setVisibility();
    this._resetActiveItem();
    if (this.activeOption) {
      this.activeOption.scrollIntoView();
    }
  }

  private _resetActiveItem(): void {
    const index = this.padAutocomplete.getOptionIndex(this._previousOptionValue);
    this.padAutocomplete.clear(null, true);
    if (index !== -1) {
      this.padAutocomplete.setActiveOption(index);
      this.padAutocomplete.activeOption?.select(false);
    } else {
      this.padAutocomplete.setActiveOption(
        this.padAutocomplete.autoActiveFirstOption ? 0 : -1
      );
    }
  }

  private _setTriggerValue(value: PadSafeAny): void {
    const option = this.padAutocomplete.getOption(value);
    const display = option ? option.label : value;
    this._previousOptionValue = option ? option.value : null;

    this._elementRef.nativeElement.value = display != null ? display : "";
    if (!this.padAutocomplete.backfill) {
      this._previousValue = display;
    }
  }

  private _getConnectedElement() {
    return this._elementRef;
  }

  private _getHostWidth(): number {
    return this._getConnectedElement().nativeElement.getBoundingClientRect().width;
  }

  private _subscribeOptionsChange(): void {
    this._optionsChangeSubscription = this.padAutocomplete.options.changes
      .pipe(tap(() => this._positionStrategy.reapplyLastPosition()))
      .subscribe(() => {
        console.log("autocomplete trigger options change emit");

        this._resetActiveItem();
        if (this.panelOpen) {
          this._overlayRef?.updatePosition();
        }
      });
  }

  private _subscribeSelectionChange(): void {
    this._selectionChangeSubscription.unsubscribe();

    this._selectionChangeSubscription = this.padAutocomplete.onSelectedChange.subscribe(
      option => {
        this._setTriggerValue(option.value);
        this.onChange(option.value);
        this._elementRef.nativeElement.focus();
        this.closePanel();
      }
    );
  }

  private _subscribeOverlayOutsideClick(): void {
    if (this._overlayRef) {
      this._overlayOutsideClickSubscription.unsubscribe();

      this._overlayOutsideClickSubscription = this._overlayRef
        .outsidePointerEvents()
        .pipe(
          filter((e: MouseEvent) => !this._elementRef.nativeElement.contains(e.target))
        )
        .subscribe(() => {
          this.closePanel();
        });
    }
  }

  private _doBackfill(): void {
    if (this.padAutocomplete.backfill && this.padAutocomplete.activeOption) {
      this._setTriggerValue(this.padAutocomplete.activeOption.displayText);
    }
  }

  private _canOpen(): boolean {
    const element: HTMLInputElement = this._elementRef.nativeElement;
    return !element.readOnly && !element.disabled;
  }
}
