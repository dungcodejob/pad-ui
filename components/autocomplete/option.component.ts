// Angular
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
  booleanAttribute,
  inject,
} from "@angular/core";

// Third parties
import { PadSafeAny } from "pad-ui-lib/core/type";

export class PadOptionSelectionChange {
  constructor(
    public source: PadOptionComponent,
    public isUserInput: boolean = false
  ) {}
}

@Component({
  selector: "pad-option",
  exportAs: "padOption",
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  template: ` <div>
    <ng-content></ng-content>
  </div>`,
  styleUrls: ["./option.component.scss"],
  standalone: true,
  host: {
    class: "pad-select-item-option",
    "[class.pad-select-item-option-selected]": "selected",
    "[class.pad-select-item-option-active]": "activated",
    "[class.pad-select-item-option-disabled]": "disabled",
    "(click)": "selectViaInteraction()",
  },
})
export class PadOptionComponent {
  private readonly _cdr = inject(ChangeDetectorRef);
  private readonly _elementRef = inject(ElementRef);

  @Input() value: PadSafeAny;
  @Input() label?: string;
  @Input({ transform: booleanAttribute }) disabled!: boolean;

  // eslint-disable-next-line @angular-eslint/no-output-on-prefix
  @Output() onSelectionChange = new EventEmitter<PadOptionSelectionChange>();

  activated = false;
  selected = false;

  get displayText() {
    return this.label || this.value.toString();
  }

  scrollIntoView() {
    const node = this._elementRef.nativeElement as HTMLElement;
    node.scrollIntoView(false);
  }

  selectViaInteraction(): void {
    if (this.disabled) {
      return;
    }

    this.selected = !this.selected;

    if (this.selected) {
      this.active();
    } else {
      this.inactive();
    }

    const event = new PadOptionSelectionChange(this, true);
    this.onSelectionChange.emit(event);
    this._cdr.markForCheck();
  }

  select(emit: boolean = true) {
    this.selected = true;
    this._cdr.markForCheck();
    if (emit) {
      const event = new PadOptionSelectionChange(this, true);
      this.onSelectionChange.emit(event);
    }
  }

  deselect(): void {
    this.selected = false;
    this._cdr.markForCheck();

    const event = new PadOptionSelectionChange(this, false);
    this.onSelectionChange.emit(event);
  }

  /** Set active (only styles) */
  active(): void {
    if (!this.activated) {
      this.activated = true;
      this._cdr.markForCheck();
    }
  }

  /** Unset active (only styles) */
  inactive(): void {
    if (this.activated) {
      this.activated = false;
      this._cdr.markForCheck();
    }
  }
}
