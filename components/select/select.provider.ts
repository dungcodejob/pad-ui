import { EnvironmentProviders, Provider, makeEnvironmentProviders } from "@angular/core";
import { PadSelectComponent } from "./select.component";

export const providePadSelect = (): EnvironmentProviders => {
  const providers: Provider[] = [PadSelectComponent];

  return makeEnvironmentProviders(providers);
};
