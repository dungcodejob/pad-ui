import { NgModule } from "@angular/core";
import { PadIconModule } from "pad-ui-lib/icon";
import { PadSelectComponent } from "./select.component";

@NgModule({
  imports: [PadIconModule, PadSelectComponent],
  providers: [],
  exports: [PadSelectComponent],
})
export class PadSelectModule {}
