import { NgIf } from "@angular/common";

import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
  forwardRef,
} from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
import { OnChangeType, OnTouchedType, PadSafeAny } from "pad-ui-lib/core/type";
import { PadIconModule } from "pad-ui-lib/icon";

@Component({
  selector: "pad-select",
  styleUrls: ["./select.scss"],
  templateUrl: "./select.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [NgIf, PadIconModule],
  host: {
    class: "pad-select",
  },
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PadSelectComponent),
      multi: true,
    },
  ],
})
export class PadSelectComponent implements ControlValueAccessor {
  @Input() padPlaceholder?: string;
  @Input() display: PadSafeAny;

  @Input()
  get value() {
    return this._value;
  }
  set value(val: PadSafeAny) {
    if (val !== undefined && this.value !== val) {
      this._value = val;
      this._onChanged(val);
      this._onTouched(val);
    }
  }
  private _value: PadSafeAny;

  disabled = false;

  private _onChanged!: OnChangeType;
  private _onTouched!: OnTouchedType;

  writeValue(obj: PadSafeAny): void {
    this.value = obj;
  }
  registerOnChange(fn: PadSafeAny): void {
    this._onChanged = fn;
  }
  registerOnTouched(fn: PadSafeAny): void {
    this._onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }
}
