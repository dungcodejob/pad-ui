import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from "@angular/core";

export type PadFeaturedIconSize = "lg" | "md" | "sm";

@Component({
  selector: "pad-featured-icon",
  exportAs: "padFeaturedIcon",
  template: `<ng-content></ng-content>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./featured-icon.scss"],
  host: {
    class: "pad-featured-icon",
    "[class.pad-featured-icon-size-sm]": `padSize === 'sm'`,
    "[class.pad-featured-icon-size-md]": `padSize === 'md'`,
    "[class.pad-featured-icon-size-lg]": `padSize === 'lg'`,
  },
})
export class PadFeaturedIconComponent {
  @Input() padSize: PadFeaturedIconSize = "md";
}
