import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { ModuleWithProviders, NgModule } from "@angular/core";
import { PadFeaturedIconComponent } from "./featured-icon.component";
import { PadIconComponent } from "./icon.component";
import { PadIconRegistry } from "./icon.registry";
import { PAD_ICON_CONFIG, PadIconConfig } from "./icon.type";

@NgModule({
  declarations: [PadFeaturedIconComponent, PadIconComponent],
  providers: [PadIconRegistry],
  exports: [PadFeaturedIconComponent, PadIconComponent],
  imports: [CommonModule, HttpClientModule],
})
export class PadIconModule {
  static forRoot(
    config: Partial<PadIconConfig> = {}
  ): ModuleWithProviders<PadIconModule> {
    return {
      ngModule: PadIconModule,
      providers: [
        {
          provide: PAD_ICON_CONFIG,
          useValue: config,
        },
      ],
    };
  }
}
