import { InjectionToken } from "@angular/core";

export interface PadIconConfig {
  defaultColor: string;
  defaultSize: PadIconSize;
  path: string;
  sizes: GroupIconSize;
}

export type PadIconSize = "xs" | "sm" | "md" | "lg" | "xl" | "2xl";

export type GroupIconSize = {
  [k in PadIconSize]: string;
} & Record<string, string>;

export const DEFAULT_CONFIG: PadIconConfig = {
  sizes: {
    xs: "0.75rem",
    sm: "1rem",
    md: "1.25rem",
    lg: "1.5rem",
    xl: "2rem",
    "2xl": "2.5rem",
  },
  path: "/assets/icons/",
  defaultColor: "",
  defaultSize: "md",
};

export const PAD_ICON_CONFIG = new InjectionToken<PadIconConfig>("PAD_ICON_CONFIG");
