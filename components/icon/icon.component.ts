import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Inject,
  Input,
  OnChanges,
  SimpleChanges,
  ViewEncapsulation,
} from "@angular/core";
import { Observable, map, tap } from "rxjs";
import { PadIconRegistry } from "./icon.registry";
import { DEFAULT_CONFIG, PAD_ICON_CONFIG, PadIconConfig, PadIconSize } from "./icon.type";

@Component({
  selector: "pad-icon",
  templateUrl: "./icon.component.html",
  styleUrls: ["./icon.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: "pad-icon",
    "[class.pad-icon-size-xs]": `padSize === 'xs'`,
    "[class.pad-icon-size-sm]": `padSize === 'sm'`,
    "[class.pad-icon-size-md]": `padSize === 'md'`,
    "[class.pad-icon-size-lg]": `padSize === 'lg'`,
    "[class.pad-icon-size-xl]": `padSize === 'xl'`,
    "[class.pad-icon-size-2xl]": `padSize === '2xl'`,
  },
})
export class PadIconComponent implements OnChanges {
  @Input() key!: string;
  @Input() padSize: PadIconSize = "md";
  @Input() strokeWidth!: string | number;
  @Input() width!: string | number;
  @Input() height!: string | number;

  private _init: boolean;
  private _config: PadIconConfig;

  svgIcon$!: Observable<SVGElement>;

  get element(): HTMLElement {
    return this._elementRef.nativeElement;
  }

  constructor(
    private _elementRef: ElementRef<HTMLElement>,
    private _registry: PadIconRegistry,

    @Inject(PAD_ICON_CONFIG) customConfig: PadIconConfig
  ) {
    this._init = true;
    this._config = { ...DEFAULT_CONFIG, ...customConfig };
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { key, size, width, height } = changes;
    if (key) {
      this.svgIcon$ = this._setSvgIcon(key.currentValue);
    }

    if (size) {
      this._setIconSize(size.currentValue);
    }

    if (this._init && !size?.currentValue) {
      this._setIconSize(DEFAULT_CONFIG.defaultSize);
    }

    if (width) {
      this._setIconWidth(width.currentValue);
    }

    if (height) {
      this._setIconHeight(height.currentValue);
    }

    this._init = false;
  }

  private _setSvgIcon(key: string) {
    return this._registry.get(key).pipe(
      map(svgElement => this._setSvgElementAttribute(svgElement)),
      map(svgElement => svgElement.cloneNode(true) as SVGElement),
      tap(cloneElement => {
        this.element.innerHTML = "";
        this.element.appendChild(cloneElement);
      })
    );
  }

  private _setSvgElementAttribute(svgElement: SVGElement) {
    if (this.strokeWidth) {
      svgElement.style.strokeWidth = this._coerceCssPixelValue(this.strokeWidth);
    }
    return svgElement;
  }

  private _setIconWidth(width: string | number) {
    const value = this._coerceCssPixelValue(width);
    this.element.style.width = value;
  }

  private _setIconHeight(height: string | number) {
    const value = this._coerceCssPixelValue(height);
    this.element.style.height = value;
  }

  private _setIconSize(size: PadIconSize) {
    const value = this._coerceCssPixelValue(this._config.sizes[size]);
    this.element.style.setProperty(`--pad-icon-size-${size}`, value);
  }

  private _coerceCssPixelValue(value: number | string) {
    if (value == null) {
      return "";
    }
    if (typeof value === "number") {
      return `${value}px`;
    }
    return value;
  }
}
