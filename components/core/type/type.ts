// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type PadSafeAny = any;
export type CompareWith = (o1: PadSafeAny, o2: PadSafeAny) => boolean;
export type PadComponentClass = `pad-${string}`;
export type OnTouchedType<T = PadSafeAny> = (value?: T) => PadSafeAny;
export type OnChangeType<T = PadSafeAny> = (value: T) => void;
