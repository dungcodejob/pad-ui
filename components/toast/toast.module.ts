import { ModuleWithProviders, NgModule } from "@angular/core";
import { PadToastContainerComponent } from "./components/toast-container.component";
import { PadToastComponent } from "./components/toast.component";

import { PadToastConfig } from "./toast-config";
import { PAD_TOAST_CONFIG } from "./toast-injector";
import { PadToastService } from "./toast.service";

@NgModule({
  imports: [PadToastComponent, PadToastContainerComponent],
  providers: [PadToastService],
  exports: [PadToastComponent],
})
export class PadToastModule {
  static forRoot(config?: Partial<PadToastConfig>): ModuleWithProviders<PadToastModule> {
    return {
      ngModule: PadToastModule,
      providers: [
        {
          provide: PAD_TOAST_CONFIG,
          useValue: config ? config : new PadToastConfig(),
        },
      ],
    };
  }
}
