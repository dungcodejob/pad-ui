import { PadToastContainerComponent } from "./components/toast-container.component";

export class PadToastRef {
  id = Symbol();
  readonly #container: PadToastContainerComponent;

  constructor(container: PadToastContainerComponent) {
    this.#container = container;
  }

  close() {
    this.#container.remove(this.id);
  }
}
