export enum PadToastType {
  Success,
  Error,
  Info,
  Warning,
}

export interface PadToastData {
  type: PadToastType;
  title?: string;
  message?: string;
  iconKey?: string;
  width?: number | string;
}
