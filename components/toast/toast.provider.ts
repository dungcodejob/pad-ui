import { CommonModule } from "@angular/common";
import { EnvironmentProviders, Provider, makeEnvironmentProviders } from "@angular/core";

import { PadToastConfig } from "./toast-config";
import { PAD_TOAST_CONFIG } from "./toast-injector";
import { PadToastService } from "./toast.service";

export const providePadToast = (
  config: Partial<PadToastConfig> = {}
): EnvironmentProviders => {
  const defaultConfig = new PadToastConfig();

  const providers: Provider[] = [
    CommonModule,
    PadToastService,
    {
      provide: PAD_TOAST_CONFIG,
      useValue: { ...config, ...defaultConfig },
    },
  ];

  return makeEnvironmentProviders(providers);
};
