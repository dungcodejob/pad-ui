import { ComponentType } from "@angular/cdk/portal";
import { PadToastComponent } from "./components/toast.component";
export class PadToastConfig {
  /**
   * toast time to live in milliseconds
   * default: 5000
   */
  timeout = 5000;

  /**
   * Angular toast component to be shown
   * default: Toast
   */
  component: ComponentType<unknown> = PadToastComponent;
  /**
   * animation ease time on toast
   * default: 300
   */
  easeTime = 200;
  /**
   * animation easing on toast
   * default: ease-in
   */
  easing = "ease-in";
}
