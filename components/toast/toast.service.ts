import { Overlay } from "@angular/cdk/overlay";
import { ComponentPortal } from "@angular/cdk/portal";
import { Injectable, Injector, inject } from "@angular/core";
import { PadToastContainerComponent } from "./public-api";
import { PadToastConfig } from "./toast-config";
import { PadToastData, PadToastType } from "./toast-data";
import { PAD_TOAST_CONFIG } from "./toast-injector";

type PadToastParams = Omit<PadToastData, "type">;

@Injectable()
export class PadToastService {
  readonly #globalConfig = inject(PAD_TOAST_CONFIG);
  readonly #overlay = inject(Overlay);
  readonly #injector = inject(Injector);
  #container?: PadToastContainerComponent;

  info(params: PadToastParams, config?: Partial<PadToastConfig>) {
    return this.#buildToast({ type: PadToastType.Info, ...params }, config);
  }

  warning(params: PadToastParams, config?: Partial<PadToastConfig>) {
    return this.#buildToast({ type: PadToastType.Warning, ...params }, config);
  }

  error(params: PadToastParams, config?: Partial<PadToastConfig>) {
    return this.#buildToast({ type: PadToastType.Error, ...params }, config);
  }

  success(params: PadToastParams, config?: Partial<PadToastConfig>) {
    return this.#buildToast({ type: PadToastType.Success, ...params }, config);
  }

  #buildToast(data: PadToastData, config?: Partial<PadToastConfig>) {
    const container = this.#getContainer();
    return container.create(data, config);
  }

  #getContainer() {
    if (this.#container) {
      return this.#container;
    }

    const overlayRef = this.#overlay.create({
      // hasBackdrop: false,
      // scrollStrategy: this.#overlay.scrollStrategies.noop(),
      positionStrategy: this.#overlay
        .position()
        .global()
        .top(20 + "px")
        .right(20 + "px"),
    });

    const injector = this.#createContainerInjector(this.#globalConfig, this.#injector);
    const containerPortal = new ComponentPortal(
      PadToastContainerComponent,
      null,
      injector
    );

    const containerRef = overlayRef.attach(containerPortal);
    this.#container = containerRef.instance;
    return containerRef.instance;
  }

  #createContainerInjector(config: PadToastConfig, parentInjector: Injector): Injector {
    return Injector.create({
      providers: [
        {
          provide: PAD_TOAST_CONFIG,
          useValue: config,
        },
      ],
      parent: parentInjector,
    });
  }
}
