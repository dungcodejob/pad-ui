import { InjectionToken } from "@angular/core";
import { PadToastConfig } from "./toast-config";
import { PadToastData } from "./toast-data";

export const PAD_TOAST_CONFIG = new InjectionToken<PadToastConfig>("PadToastConfig");
export const PAD_TOAST_DATA = new InjectionToken<PadToastData>("PadToastData");
