import { ComponentPortal, Portal, PortalModule } from "@angular/cdk/portal";
import { NgFor } from "@angular/common";
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  ViewEncapsulation,
  inject,
  signal,
} from "@angular/core";
import { PadToastConfig } from "../toast-config";
import { PadToastData } from "../toast-data";
import { PAD_TOAST_CONFIG, PAD_TOAST_DATA } from "../toast-injector";
import { PadToastRef } from "../toast-ref";

export interface ActiveToast {
  id: symbol;
  data: PadToastData;
  config?: PadToastConfig;
  ref: PadToastRef;
  portal: Portal<unknown>;
}

@Component({
  selector: "pad-toast-container",
  styleUrls: ["./toast-container.component.scss"],
  templateUrl: "toast-container.component.html",
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  host: {
    class: "pad-toast-container",
  },
  imports: [PortalModule, NgFor],
})
export class PadToastContainerComponent {
  readonly #globalConfig = inject(PAD_TOAST_CONFIG);
  readonly #injector = inject(Injector);
  readonly toasts = signal<ActiveToast[]>([]);

  create(data: PadToastData, config?: Partial<PadToastConfig>): PadToastRef {
    const mergeConfig = this.#getMergeConfig(config);
    const ref = new PadToastRef(this);
    const injector = this.#createInjector(data, mergeConfig, ref, this.#injector);
    const portal = new ComponentPortal(mergeConfig.component, null, injector);

    this.toasts.mutate(toasts =>
      toasts.push({ id: ref.id, ref, data, config: mergeConfig, portal })
    );

    return ref;
  }

  remove(id: symbol) {
    this.toasts.update(toasts => toasts.filter(toast => toast.id !== id));
  }

  #getMergeConfig(config?: Partial<PadToastConfig>): PadToastConfig {
    if (config == null) {
      return this.#globalConfig;
    }

    return { ...this.#globalConfig, ...config };
  }

  #createInjector(
    data: PadToastData,
    config: PadToastConfig,
    ref: PadToastRef,
    parentInjector: Injector
  ): Injector {
    return Injector.create({
      providers: [
        {
          provide: PAD_TOAST_CONFIG,
          useValue: config,
        },
        {
          provide: PAD_TOAST_DATA,
          useValue: data,
        },
        {
          provide: PadToastRef,
          useValue: ref,
        },
      ],
      parent: parentInjector,
    });
  }
}
