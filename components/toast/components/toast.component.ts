import { AnimationEvent } from "@angular/animations";
import { NgIf } from "@angular/common";
import {
  ChangeDetectionStrategy,
  Component,
  DestroyRef,
  OnInit,
  ViewEncapsulation,
  inject,
  signal,
} from "@angular/core";
import { takeUntilDestroyed } from "@angular/core/rxjs-interop";
import { PadButtonModule } from "pad-ui-lib/button";
import { PadIconModule } from "pad-ui-lib/icon";
import { take, timer } from "rxjs";
import { notificationMotion } from "../toast-animation";
import { PadToastType } from "../toast-data";
import { PAD_TOAST_CONFIG, PAD_TOAST_DATA } from "../toast-injector";
import { PadToastRef } from "../toast-ref";
@Component({
  selector: "pad-toast",
  styleUrls: ["./toast.component.scss"],
  templateUrl: "./toast.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  animations: [notificationMotion],
  standalone: true,
  imports: [NgIf, PadIconModule, PadButtonModule],
})
export class PadToastComponent implements OnInit {
  readonly data = inject(PAD_TOAST_DATA);
  readonly iconKey: string;
  readonly toastClasses: string;
  readonly #destroyRef = inject(DestroyRef);
  readonly #config = inject(PAD_TOAST_CONFIG);
  readonly #ref = inject(PadToastRef);

  color: "gray" | "error" | "warning" | "success" = "gray";
  state = signal<{
    value: "enterRight" | "leave";
    params: { easeTime: number | string; easing: string };
  }>({
    value: "enterRight",
    params: { easeTime: this.#config.easeTime, easing: "ease" },
  });
  constructor() {
    const classes = ["pad-toast"];
    this.iconKey = "check-circle";
    if (this.data.type == PadToastType.Success) {
      classes.push("pad-toast-success");
      this.iconKey = "check-circle";
      this.color = "success";
    }

    if (this.data.type == PadToastType.Error) {
      classes.push("pad-toast-error");
      this.iconKey = "alert-circle";
      this.color = "error";
    }

    if (this.data.type == PadToastType.Warning) {
      classes.push("pad-toast-warning");
      this.iconKey = "alert-circle";
      this.color = "warning";
    }

    if (this.data.type == PadToastType.Info) {
      classes.push("pad-toast-info");
      this.iconKey = "alert-circle";
      this.color = "gray";
    }

    if (!this.data.title) {
      classes.push("not-title");
    }
    this.toastClasses = classes.join(" ");
  }

  ngOnInit() {
    timer(this.#config.timeout)
      .pipe(take(1), takeUntilDestroyed(this.#destroyRef))
      .subscribe(() => this.#close());
  }

  onCloseClick() {
    this.#close();
  }

  animationDone(event: AnimationEvent) {
    if (event.toState == "leave") {
      this.#ref.close();
    }
  }

  #close() {
    // update animation state
    this.state.update(state => ({ ...state, value: "leave" }));

    // waiting for animation run to end, then close toast
    // timer(this.#config.easeTime)
    //   .pipe(take(1), takeUntilDestroyed(this.#destroyRef))
    //   .subscribe(() => this.#ref.close());
  }
}
