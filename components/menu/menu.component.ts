import { ChangeDetectionStrategy, Component, ViewEncapsulation } from "@angular/core";

@Component({
  selector: "pad-menu",
  exportAs: "padMenu",
  template: `<ng-content></ng-content>`,
  styleUrls: ["./menu.scss"],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  host: {
    class: "pad-menu",
  },
})
export class PadMenuComponent {}
