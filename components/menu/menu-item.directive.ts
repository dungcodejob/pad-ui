import { Directive } from "@angular/core";

@Directive({
  selector: "[pad-menu-item]",
  exportAs: "padMenuItem",
  standalone: true,
  host: {
    class: "pad-menu-item",
  },
})
export class PadMenuItemDirective {}
