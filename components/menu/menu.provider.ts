import { EnvironmentProviders, Provider, makeEnvironmentProviders } from "@angular/core";
import { PadMenuDividerDirective } from "./menu-divider.directive";
import { PadMenuItemDirective } from "./menu-item.directive";
import { PadMenuComponent } from "./menu.component";

export const providePadRadio = (): EnvironmentProviders => {
  const providers: Provider[] = [
    PadMenuComponent,
    PadMenuItemDirective,
    PadMenuDividerDirective,
  ];

  return makeEnvironmentProviders(providers);
};
