import { Directive } from "@angular/core";

@Directive({
  selector: "[pad-menu-divider]",
  exportAs: "padMenuDivider",
  standalone: true,
  host: {
    class: "pad-menu-divider",
  },
})
export class PadMenuDividerDirective {}
