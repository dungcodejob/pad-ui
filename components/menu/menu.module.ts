import { NgModule } from "@angular/core";
import { PadMenuDividerDirective } from "./menu-divider.directive";
import { PadMenuItemDirective } from "./menu-item.directive";
import { PadMenuComponent } from "./menu.component";

@NgModule({
  imports: [PadMenuComponent, PadMenuItemDirective, PadMenuDividerDirective],
  declarations: [],
  exports: [PadMenuComponent, PadMenuItemDirective, PadMenuDividerDirective],
})
export class PadMenuModule {}
