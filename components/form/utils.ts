export type PadFormControlStatus = "error" | "success" | "pending" | null;

export function generatorStatusClass(
  prefix: string,
  status: NonNullable<PadFormControlStatus>
) {
  return `${prefix}-status-${status}`;
}

export function transformControlStatus(origin: string | null): PadFormControlStatus {
  switch (origin) {
    case "VALID":
      return "success";
    case "INVALID":
      return "error";
    case "PENDING":
      return "pending";
    default:
      return null;
  }
}
