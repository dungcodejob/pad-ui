import { BooleanInput, coerceBooleanProperty } from "@angular/cdk/coercion";
import {
  Directive,
  ElementRef,
  Input,
  OnDestroy,
  OnInit,
  Renderer2,
  inject,
} from "@angular/core";
import { NgControl } from "@angular/forms";
import {
  BehaviorSubject,
  Subject,
  distinctUntilChanged,
  filter,
  map,
  takeUntil,
  tap,
} from "rxjs";
import {
  PadFormControlStatus,
  generatorStatusClass,
  transformControlStatus,
} from "../utils";

export type PadInputSize = "lg" | "md";

@Directive({
  selector: "input[padInput], textarea[padInput]",
  exportAs: "padInput",
  host: {
    class: "pad-input",
    "[class.pad-input-disabled]": "disabled",
    "[disabled]": "disabled",
    "[class.pad-input-size-md]": `padSize === 'md'`,
    "[class.pad-input-size-lg]": `padSize === 'lg'`,
  },
})
export class PadInputDirective implements OnInit, OnDestroy {
  protected readonly control = inject(NgControl, {
    self: true,
    optional: true,
  });
  protected readonly renderer = inject(Renderer2);
  protected readonly elementRef = inject(ElementRef);

  @Input() padSize: PadInputSize = "lg";
  @Input()
  get disabled() {
    if (this.control && this.control.disabled != null) {
      return this.control.disabled;
    }

    return this._disabled;
  }
  set disabled(value: BooleanInput) {
    this._disabled = coerceBooleanProperty(value);
  }
  protected _disabled = false;

  protected readonly _statusSubject = new BehaviorSubject<PadFormControlStatus>(null);
  protected readonly _destroy$ = new Subject<void>();

  status$ = this._statusSubject.asObservable();

  ngOnInit(): void {
    if (this.control && this.control.statusChanges) {
      this.control.statusChanges
        .pipe(
          filter(() => (this.control?.dirty || this.control?.touched) ?? false),
          distinctUntilChanged(),
          map(value => transformControlStatus(value)),
          tap(status => this.statusStylesChange(status)),
          tap(status => this._statusSubject.next(status)),
          takeUntil(this._destroy$)
        )
        .subscribe();
    }
  }

  protected statusStylesChange(nextStatus: PadFormControlStatus) {
    const prefix = "pad-input";
    const oldStatus = this._statusSubject.value;
    if (oldStatus) {
      const oldStatusClass = generatorStatusClass(prefix, oldStatus);
      this.renderer.removeClass(this.elementRef.nativeElement, oldStatusClass);
    }
    if (nextStatus) {
      const nextStatusClass = `${prefix}-status-${nextStatus}`;
      this.renderer.addClass(this.elementRef.nativeElement, nextStatusClass);
    }
  }

  ngOnDestroy(): void {
    this._destroy$.next();
    this._destroy$.complete();
  }
}
