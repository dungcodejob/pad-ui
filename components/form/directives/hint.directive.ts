import {
  Directive,
  ElementRef,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewRef,
  inject,
} from "@angular/core";
import { Subscription } from "rxjs";
import { PadFormControlComponent } from "../components/form-control.component";
@Directive({
  selector: "pad-hint",
  exportAs: "padHint",
  host: {
    class: "pad-hint",
  },
})
export class PadHintDirective implements OnInit, OnDestroy {
  private readonly root = inject(PadFormControlComponent);
  private readonly elementRef = inject(ElementRef);
  private readonly renderer = inject(Renderer2);

  private _view?: ViewRef;
  private _subscription?: Subscription;

  ngOnInit(): void {
    this._subscription = this.root.status$.subscribe(status => {
      if (status === "error") {
        this.renderer.setStyle(this.elementRef.nativeElement, "display", "none");
      } else {
        this.renderer.setStyle(this.elementRef.nativeElement, "display", "block");
      }
    });
  }

  ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }
}
