import { Directive, ElementRef } from "@angular/core";
@Directive({
  selector: "[padSuffix]",
  exportAs: "padSuffix",
  host: {
    class: "pad-suffix",
  },
})
export class PadSuffixDirective {
  constructor(private _elementRef: ElementRef<HTMLElement>) {}

  getWidth() {
    return this._elementRef.nativeElement.offsetWidth;
  }
}
