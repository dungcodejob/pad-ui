import { Directive, ElementRef } from "@angular/core";
@Directive({
  selector: "[padPrefix]",
  exportAs: "padPrefix",
  host: {
    class: "pad-prefix",
  },
})
export class PadPrefixDirective {
  constructor(private _elementRef: ElementRef<HTMLElement>) {}

  getWidth() {
    return this._elementRef.nativeElement.offsetWidth;
  }
}
