import {
  Directive,
  EmbeddedViewRef,
  Input,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewContainerRef,
  inject,
} from "@angular/core";
import { PadSafeAny } from "pad-ui-lib/core/type";
import {
  EMPTY,
  ReplaySubject,
  Subscription,
  combineLatest,
  distinctUntilChanged,
  filter,
  map,
  switchMap,
} from "rxjs";
import { PadFormControlComponent } from "../components/form-control.component";

export interface HasErrorContext {
  $implicit: PadSafeAny;
}

@Directive({
  selector: "[hasError]",
})
export class HasErrorDirective implements OnInit, OnDestroy {
  private readonly root = inject(PadFormControlComponent);
  private readonly templateRef = inject<TemplateRef<HasErrorContext>>(
    TemplateRef<HasErrorContext>
  );
  private readonly vcr = inject(ViewContainerRef);

  @Input()
  set hasError(value: string) {
    this._errorName$.next(value);
  }

  private readonly _errorName$ = new ReplaySubject<string>(1);
  private readonly _ngControl$ = this.root.ngControl$;

  private _view?: EmbeddedViewRef<HasErrorContext>;
  private _subscription?: Subscription;

  ngOnInit(): void {
    const status$ = this._ngControl$.pipe(
      switchMap(ngControl =>
        (ngControl.statusChanges || EMPTY).pipe(
          filter(() => (ngControl.dirty || ngControl.touched) ?? false)
        )
      )
    );

    const error$ = combineLatest([this._ngControl$, this._errorName$, status$]).pipe(
      map(([ngControl, errorName]) => ({
        hasError: ngControl.hasError(errorName),
        value: ngControl.getError(errorName),
      })),
      distinctUntilChanged((x, y) => x.hasError === y.hasError && x.value === y.value)
    );

    this._subscription = error$.subscribe(error => {
      if (!error.hasError) {
        this.vcr.clear();
        this._view?.destroy();
        this._view = undefined;
        return;
      }

      if (this._view) {
        this._view.context.$implicit = error.value;
        this._view.markForCheck();
        return;
      }

      this._view = this.vcr.createEmbeddedView(this.templateRef, {
        $implicit: error.value,
      });
    });
  }

  ngOnDestroy(): void {
    this._subscription?.unsubscribe();
  }
}
