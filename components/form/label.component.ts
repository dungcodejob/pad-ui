import { BooleanInput, coerceBooleanProperty } from "@angular/cdk/coercion";
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from "@angular/core";

@Component({
  selector: "pad-label",
  template: `<label [attr.for]="padFor" [class.pad-label-required]="required">
    <ng-content></ng-content>
  </label>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./label.component.scss"],
  host: {
    "[required]": "required",
    class: "pad-label",
  },
})
export class PadLabelComponent {
  // TODO: change input bindings name not be aliased
  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input("for") padFor = "";

  @Input()
  get required() {
    return this.#required;
  }
  set required(value: BooleanInput) {
    this.#required = coerceBooleanProperty(value);
  }
  #required = false;
}
