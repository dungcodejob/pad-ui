import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { PadFormControlComponent } from "./components/form-control.component";
import { PadFormFieldComponent } from "./components/form-field.component";
import { HasErrorDirective } from "./directives/has-error.directive";
import { PadHintDirective } from "./directives/hint.directive";
import { PadInputDirective } from "./directives/input.directive";
import { PadPrefixDirective } from "./directives/prefix.directive";
import { PadSuffixDirective } from "./directives/suffix.directive";
import { PadErrorComponent } from "./error.component";
import { PadLabelComponent } from "./label.component";

@NgModule({
  declarations: [
    PadFormFieldComponent,
    PadFormControlComponent,
    PadInputDirective,
    PadLabelComponent,
    PadErrorComponent,
    HasErrorDirective,
    PadHintDirective,
    PadPrefixDirective,
    PadSuffixDirective,
  ],
  exports: [
    PadFormFieldComponent,
    PadFormControlComponent,
    PadLabelComponent,
    PadInputDirective,
    PadErrorComponent,
    PadHintDirective,
    HasErrorDirective,
    PadPrefixDirective,
    PadSuffixDirective,
  ],
  imports: [CommonModule, ReactiveFormsModule],
})
export class PadFormModule {}
