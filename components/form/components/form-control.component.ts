import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  DestroyRef,
  ElementRef,
  OnInit,
  Renderer2,
  ViewEncapsulation,
  inject,
} from "@angular/core";
import { takeUntilDestroyed } from "@angular/core/rxjs-interop";
import { FormControlDirective, FormControlName, NgControl } from "@angular/forms";

import {
  BehaviorSubject,
  EMPTY,
  ReplaySubject,
  distinctUntilChanged,
  filter,
  map,
  switchMap,
  tap,
} from "rxjs";
import { PadPrefixDirective } from "../directives/prefix.directive";
import { PadSuffixDirective } from "../directives/suffix.directive";
import {
  PadFormControlStatus,
  generatorStatusClass,
  transformControlStatus,
} from "../utils";

@Component({
  selector: "pad-form-control",
  exportAs: "padFormControl",
  template: `<ng-content></ng-content>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  styleUrls: ["./form-control.scss"],
  host: {
    class: "pad-form-control",
  },
})
export class PadFormControlComponent implements OnInit {
  protected readonly _renderer = inject(Renderer2);
  protected readonly _destroyRef = inject(DestroyRef);
  elementRef = inject(ElementRef);

  @ContentChild(NgControl, { static: false })
  set ngControl(value: FormControlDirective | FormControlName) {
    this._ngControl$.next(value);
  }

  get ngControl$() {
    return this._ngControl$.asObservable();
  }
  private readonly _ngControl$ = new ReplaySubject<
    FormControlDirective | FormControlName
  >(1);

  @ContentChild(PadPrefixDirective, { static: false })
  set prefix(value: PadPrefixDirective) {
    const element = this.elementRef.nativeElement as HTMLElement;
    if (value) {
      element.style.setProperty(
        "--prefix-width",
        value.getWidth() ? value.getWidth() + "px" : "1.25rem"
      );
      element.classList.add("has-prefix");
    } else {
      element.classList.remove("has-prefix");
    }
  }

  @ContentChild(PadSuffixDirective, { static: false })
  set suffix(value: PadSuffixDirective) {
    const element = this.elementRef.nativeElement as HTMLElement;
    if (value) {
      element.style.setProperty(
        "--suffix-width",
        value.getWidth() ? value.getWidth() + "px" : "1.25rem"
      );

      element.classList.add("has-suffix");
    } else {
      element.classList.remove("has-suffix");
    }
  }

  protected readonly _statusSubject = new BehaviorSubject<PadFormControlStatus>(null);
  status$ = this._statusSubject.asObservable();

  ngOnInit(): void {
    this._ngControl$
      .pipe(
        switchMap(ngControl =>
          (ngControl.statusChanges || EMPTY).pipe(
            distinctUntilChanged(),
            filter(() => (ngControl.dirty || ngControl.touched) ?? false)
          )
        ),
        map(value => transformControlStatus(value)),
        tap(status => this.statusStylesChange(status)),
        tap(status => this._statusSubject.next(status)),
        takeUntilDestroyed(this._destroyRef)
      )
      .subscribe();
  }

  protected statusStylesChange(nextStatus: PadFormControlStatus) {
    const prefix = "pad-control";
    const oldStatus = this._statusSubject.value;
    if (oldStatus) {
      const oldStatusClass = generatorStatusClass(prefix, oldStatus);
      this._renderer.removeClass(this.elementRef.nativeElement, oldStatusClass);
    }
    if (nextStatus) {
      const nextStatusClass = `${prefix}-status-${nextStatus}`;
      this._renderer.addClass(this.elementRef.nativeElement, nextStatusClass);
    }
  }
}
